<?php
/*
Template Name: Cuerpo Docente
*/


// si es solicitado vía ajax responde el objeto
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest'){
	header('Content-Type:application/json');
	$page_id=get_the_ID();
	$page=get_page( $page_id );
	$page->vars=get_post_custom($page_id);
	echo json_encode( $page);
	exit();
}


get_header();
	require( ((wp_is_mobile() && ($_SESSION["m_desktop"]!=true)) ? 'mobile': 'web').'/page.cuerpodocente.php' );
get_footer();
