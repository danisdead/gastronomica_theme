<html><body style="margin:0;padding:0;"><center>
<table border="0" margin="0" padding="0" cellspacing="0" cellpadding="0" align="center">
	<tbody>
		<tr>
			<td>
				<table border="0" margin="0" padding="0" cellspacing="0" cellpadding="0" width="650px" >
					<tbody><tr>
						<td width="650px" height="28px" align="center" valign="center" style="font:normal 13px arial; color:#888888; text-align:center;">
							Si no puedes ver este mensaje, haz clic <a href="<%url%>/mailers/contact-forUser-online.php"><b>aquí</b></a>.
						</td>
					</tr>
				</tbody></table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" margin="0" padding="0" cellspacing="0" cellpadding="0" width="650px" >
					<tbody><tr>
						<td width="650px" height="22px">
							<img src="<%url%>/mailers/images/m_0.jpg" alt="" title="" style="display:block;border:0;">						</td>
					</tr>
				</tbody></table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" margin="0" padding="0" cellspacing="0" cellpadding="0" width="650px" >
					<tbody><tr>
						<td width="227px" height="76px">
							<a target="_blank" href="http://<%domain%>">
								<img src="<%url%>/mailers/images/m_1.jpg" alt="" title="" style="display:block;border:0;">						</a>
							</td>
						<td width="175px" height="76px">
							<img src="<%url%>/mailers/images/m_2.jpg" alt="" title="" style="display:block;border:0;">						</td>
						<td width="248px" height="76px" align="left" valign="center" style="font:normal 13px arial; color:#EE4123; text-align:left;">
							+52 55 5207 5443 &nbsp; &nbsp; +52 55 5207 7485
						</td>
					</tr>
				</tbody></table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" margin="0" padding="0" cellspacing="0" cellpadding="0" width="650px" >
					<tbody><tr>
						<td width="650px" height="73px">
							<img src="<%url%>/mailers/images/m_3.jpg" alt="" title="" style="display:block;border:0;">						</td>
					</tr>
				</tbody></table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" margin="0" padding="0" cellspacing="0" cellpadding="0" width="650px" >
					<tbody><tr>
						<td width="650px" height="74px">
							<img src="<%url%>/mailers/images/m_4.jpg" alt="" title="" style="display:block;border:0;">						</td>
					</tr>
				</tbody></table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" margin="0" padding="0" cellspacing="0" cellpadding="0" width="650px" >
					<tbody><tr>
						<td width="650px" height="76px">
							<img src="<%url%>/mailers/images/m_5.jpg" alt="" title="" style="display:block;border:0;">						</td>
					</tr>
				</tbody></table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" margin="0" padding="0" cellspacing="0" cellpadding="0" width="650px" >
					<tbody><tr>
						<td width="650px" height="78px">
							<img src="<%url%>/mailers/images/m_6.jpg" alt="" title="" style="display:block;border:0;">						</td>
					</tr>
				</tbody></table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" margin="0" padding="0" cellspacing="0" cellpadding="0" width="650px" >
					<tbody><tr>
						<td width="650px" height="84px">
							<img src="<%url%>/mailers/images/m_7.jpg" alt="" title="" style="display:block;border:0;">						</td>
					</tr>
				</tbody></table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" margin="0" padding="0" cellspacing="0" cellpadding="0" width="650px" >
					<tbody><tr>
						<td width="650px" height="67px" align="center" valign="top" style="font:normal 15px arial; color:#888888; text-align:center;">
							A la brevedad, uno de nuestros asesores te estará contactando para darte<br>más información sobre Gastronómica Internacional.
						</td>
					</tr>
				</tbody></table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" margin="0" padding="0" cellspacing="0" cellpadding="0" width="650px" >
					<tbody><tr>
						<td width="650px" height="58px" align="center" valign="top" style="font:normal 14px arial; color:#EE4123; text-align:center;">
							El éxito te espera. Ya diste el primer paso.
						</td>
					</tr>
				</tbody></table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" margin="0" padding="0" cellspacing="0" cellpadding="0" width="650px" >
					<tbody><tr>
						<td width="650px" height="19px">
							<img src="<%url%>/mailers/images/m_8.jpg" alt="" title="" style="display:block;border:0;">						</td>
					</tr>
				</tbody></table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" margin="0" padding="0" cellspacing="0" cellpadding="0" width="650px" >
					<tbody><tr>
						<td width="23px" height="17px">
							<img src="<%url%>/mailers/images/m_9.jpg" alt="" title="" style="display:block;border:0;">						</td>
						<td width="101px" height="17px">
							<a target="_blank" href="http://<%domain%>/oferta-educativa/">
								<img src="<%url%>/mailers/images/m_10.jpg" alt="" title="" style="display:block;border:0;">						</a>
							</td>
						<td width="40px" height="17px">
							<img src="<%url%>/mailers/images/m_11.jpg" alt="" title="" style="display:block;border:0;">						</td>
						<td width="96px" height="17px">
							<a target="_blank" href="http://<%domain%>/cuerpo-docente/">
								<img src="<%url%>/mailers/images/m_12.jpg" alt="" title="" style="display:block;border:0;">						</a>
							</td>
						<td width="40px" height="17px">
							<img src="<%url%>/mailers/images/m_13.jpg" alt="" title="" style="display:block;border:0;">						</td>
						<td width="67px" height="17px">
							<a target="_blank" href="http://<%domain%>/conocenos/quienes-somos/">
								<img src="<%url%>/mailers/images/m_14.jpg" alt="" title="" style="display:block;border:0;">						</a>
							</td>
						<td width="142px" height="17px">
							<img src="<%url%>/mailers/images/m_15.jpg" alt="" title="" style="display:block;border:0;">						</td>
						<td width="10px" height="17px">
							<a target="_blank" href="http://www.facebook.com/GastronomicaInternacional">
								<img src="<%url%>/mailers/images/m_16.jpg" alt="" title="" style="display:block;border:0;">						</a>
							</td>
						<td width="10px" height="17px">
							<img src="<%url%>/mailers/images/m_17.jpg" alt="" title="" style="display:block;border:0;">						</td>
						<td width="36px" height="17px">
							<a target="_blank" href="http://www.youtube.com/channel/UC9xKIOOQDEyL5d1xxYJgkOQ">
								<img src="<%url%>/mailers/images/m_18.jpg" alt="" title="" style="display:block;border:0;">						</a>
							</td>
						<td width="11px" height="17px">
							<img src="<%url%>/mailers/images/m_19.jpg" alt="" title="" style="display:block;border:0;">						</td>
						<td width="18px" height="17px">
							<a target="_blank" href="http://instagram.com/_gastronomica">
								<img src="<%url%>/mailers/images/m_20.jpg" alt="" title="" style="display:block;border:0;">						</a>
							</td>
						<td width="10px" height="17px">
							<img src="<%url%>/mailers/images/m_21.jpg" alt="" title="" style="display:block;border:0;">						</td>
						<td width="21px" height="17px">
							<a target="_blank" href="http://twitter.com/@_gastronomica">
								<img src="<%url%>/mailers/images/m_22.jpg" alt="" title="" style="display:block;border:0;">						</a>
							</td>
						<td width="25px" height="17px">
							<img src="<%url%>/mailers/images/m_23.jpg" alt="" title="" style="display:block;border:0;">						</td>
					</tr>
				</tbody></table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" margin="0" padding="0" cellspacing="0" cellpadding="0" width="650px" >
					<tbody><tr>
						<td width="650px" height="25px">
							<img src="<%url%>/mailers/images/m_24.jpg" alt="" title="" style="display:block;border:0;">						</td>
					</tr>
				</tbody></table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" margin="0" padding="0" cellspacing="0" cellpadding="0" width="650px" >
					<tbody><tr>
						<td width="650px" height="23px" align="center" valign="center" style="font:normal 13px arial; color:#888888; text-align:center;">
							Si recibiste este mail por equivocación, sólo bórralo por favor.
						</td>
					</tr>
				</tbody></table>
			</td>
		</tr>
	</tbody>
</table>
</body></html>