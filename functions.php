<?php
	/*if ( !session_id() )
		session_start();*/
	if ( isset( $_GET["mode"] ) )
	{
		if ( $_GET["mode"] == "desktop" )
			$_SESSION["m_desktop"] = true;
		else
			if ( $_GET["mode"] == "mobile" )
				$_SESSION["m_desktop"] = false;
	}

	if ( function_exists( 'add_theme_support' ) )
	{
		add_theme_support( 'post-thumbnails' );
	}


	function pa_registrar_opciones()
	{
		register_setting( 'pa_opciones_theme', 'pa_opciones'/*, 'pa_validar'*/ );
	}

	add_action( 'admin_init', 'pa_registrar_opciones' );

	$valores_default = array(
		'theme_logo'   => '',
		'theme_favico' => '',
		'copyright'    => '&copy;2011 Timersys',
		'logo'         => 'http://tudomain.com/images/logo_placeholder.png'
	);
	$opciones = get_option( 'pa_opciones', $valores_default );

	function pa_opciones_theme()
	{
		add_theme_page( 'Opciones Theme', 'Opciones Theme', 'edit_theme_options', 'opciones_theme', 'pa_theme_page' );
	}

	add_action( 'admin_menu', 'pa_opciones_theme' );


	function pa_theme_page()
	{
		global $valores_default;

		if ( ! isset( $_REQUEST['updated'] ) )
			$_REQUEST['updated'] = false; // Para comprobar si el formulario fue enviado o no. ?>

		<div>
			<?php screen_icon();
				echo "<h2>" . __( ' Opciones de ' ) . get_current_theme() . "</h2>";
				// Esto muestra el ícono de la página si es que tiene uno y el nombre del theme ?>

			<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
				<div><p><strong><?php _e( 'Opciones guardadas' ); ?></strong></p></div>
			<?php endif; // Si el formulario se envia mostramos el mensaje. ?>

			<form method="post" action="options.php">

				<?php $opciones = get_option( 'pa_opciones', $valores_default ); ?>

				<?php settings_fields( 'pa_opciones_theme' );
					/* Esta función imprime todos los campos hidden necesarios para que el formulario funcione correctamente */ ?>

				<table class="form-table">

					<tr>
						<th colspan="2">Definición de Páginas WEB (ID de la página correspondiente)</th>
					</tr>

					<tr valign="top">
						<th scope="row"><label for="web_home_id">ID del Home WEB</label>
						</th>
						<td><input id="web_home_id" name="pa_opciones[web_home_id]" type="text"
						           value="<?php is_array( $opciones ) ? esc_attr_e( $opciones['web_home_id'] ) : ''; ?>"/>
						</td>
					</tr>

					<tr valign="top">
						<th scope="row"><label for="web_termCond_id">ID de Térm. y Cond. WEB</label>
						</th>
						<td><input id="web_termCond_id" name="pa_opciones[web_termCond_id]" type="text"
						           value="<?php is_array( $opciones ) ? esc_attr_e( $opciones['web_termCond_id'] ) : ''; ?>"/>
						</td>
					</tr>

					<tr valign="top">
						<th scope="row"><label for="web_piltics_id">ID de Políticas WEB</label>
						</th>
						<td><input id="web_piltics_id" name="pa_opciones[web_piltics_id]" type="text"
						           value="<?php is_array( $opciones ) ? esc_attr_e( $opciones['web_piltics_id'] ) : ''; ?>"/>
						</td>
					</tr>


					<tr>
						<th colspan="2">Definición de Páginas Mobile (ID de la página correspondiente a la versión
							mobile)
						</th>
					</tr>

					<tr valign="top">
						<th scope="row"><label for="mob_home_id">ID del Home Mobile</label>
						</th>
						<td><input id="mob_home_id" name="pa_opciones[mob_home_id]" type="text"
						           value="<?php is_array( $opciones ) ? esc_attr_e( $opciones['mob_home_id'] ) : ''; ?>"/>
						</td>
					</tr>

					<tr valign="top">
						<th scope="row"><label for="mob_termCond_id">ID de Térm. y Cond. Mobile</label>
						</th>
						<td><input id="mob_termCond_id" name="pa_opciones[mob_termCond_id]" type="text"
						           value="<?php is_array( $opciones ) ? esc_attr_e( $opciones['mob_termCond_id'] ) : ''; ?>"/>
						</td>
					</tr>

					<tr valign="top">
						<th scope="row"><label for="mob_piltics_id">ID de Políticas Mobile</label>
						</th>
						<td><input id="mob_piltics_id" name="pa_opciones[mob_piltics_id]" type="text"
						           value="<?php is_array( $opciones ) ? esc_attr_e( $opciones['mob_piltics_id'] ) : ''; ?>"/>
						</td>
					</tr>

				</table>

				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e( 'Save Changes' ) ?>"/>
				</p>

			</form>

		</div>

		<?php
	}

	// renombra el blog
	add_action( 'init', 'change_author_permalinks' );
	function change_author_permalinks()
	{
		global $wp_rewrite;
		$wp_rewrite->author_base = 'blog/autor'; // Change 'member' to be the base URL you wish to use
		$wp_rewrite->author_structure = '/' . $wp_rewrite->author_base . '/%author%';
	}


	// quita el espacio superior
	remove_action( 'init', 'wp_admin_bar_init' );


	/***
	 * @author: Nathaly Alarcon enat87@gmail.com
	 */
	function registerVisitorsLog()
	{
		global $wpdb;

		if ( ! checkIfSessionIsRegisteredOnLog() )
		{
			$REMOTE_ADDR = ( isset( $_SERVER['REMOTE_ADDR'] ) ) ? $_SERVER['REMOTE_ADDR'] : "none";
			$HTTP_REFERER = ( isset( $_SERVER['HTTP_REFERER'] ) ) ? $_SERVER['HTTP_REFERER'] : "none";
			$REQUEST_URI = ( isset( $_SERVER['REQUEST_URI'] ) ) ? $_SERVER['REQUEST_URI'] : "none";

			$clientDetails = $_SERVER['HTTP_USER_AGENT'];

			$insert = "INSERT INTO VISITORS_LOG (REMOTE_ADDR, HTTP_REFERER, REQUEST_URI, SESSION_ID, DATE, CLIENT_DETAILS)
                                VALUES('" . $REMOTE_ADDR . "', '" . $HTTP_REFERER . "', '" . $REQUEST_URI . "','" . session_id() . "',  NOW(), '" . $clientDetails . "')";

			$cursos = $wpdb->query( $insert );

		}

		return $_SESSION['VISITOR_LOG'] = 'TRUE';

	}

	function checkIfSessionIsRegisteredOnLog()
	{
		global $wpdb;
		$sqlD = "SELECT *
                        FROM VISITORS_LOG
                        WHERE SESSION_ID = '" . session_id() . "'";
		$cursos = $wpdb->get_results( $sqlD );

		if ( count( $cursos ) > 0 )
		{
			return $cursos[0];
		} else
		{
			return false;
		}
	}

	function getPageDetails()
	{
		global $wpdb;

		if ( isset( $_GET['page_id'] ) )
		{
			$pageId = $_GET['page_id'];
			if ( isset( $_GET['id'] ) )
			{
				$id = $_GET['id'];

				if ( $pageId == "33" )
				{
					$table = "wp_diplomados";
				} else
				{
					$table = "wp_cursos";
				}

				$sqlD = " SELECT *
                                FROM  " . $table . "
                                WHERE 
                                id = " . $id;


			} else
			{
				$sqlD = " SELECT *
                                FROM  wp_posts
                                WHERE 
                                ID = " . $pageId;


			}
			// echo $sqlD;
			$pageDetail = $wpdb->get_results( $sqlD );

			// print_r($pageDetail);
			return $pageDetail[0];
		} else
		{
			return false;

		}

	}

	function getVieneDe()
	{
		#TO GET CURRENT PAGE NAME
		$currentPage = getPageDetails();
		if ( $currentPage )
		{
			if ( isset( $currentPage->titulo ) )
			{
				$nameCurrentPage = $currentPage->titulo;
			} else
			{
				$nameCurrentPage = $currentPage->post_title;
			}
		} else
		{
			$nameCurrentPage = "PAGINA GENERICA";
		}
		###

		### TO GET PARAMETERS ##
		$PARAMETERS = '';
		$sessionData = checkIfSessionIsRegisteredOnLog();
		$PARAMETERS = str_replace( '&', ', -', $sessionData->REQUEST_URI );
		$PARAMETERS = str_replace( '?', ', -', $PARAMETERS );
		$PARAMETERS = str_replace( '/', ', -', $PARAMETERS );
		$importantVars = array( '-keyword', '-source', '-network', '-gclid', '-utm_source', '-utm_medium', '-utm_campaign' );
		$stringVarsUrl = '';
		$varsInUrl = explode( ',', $PARAMETERS );
		foreach ( $varsInUrl as $val )
		{
			$individualVar = explode( '=', $val );
			if ( in_array( trim( $individualVar[0] ), $importantVars ) )
			{
				$stringVarsUrl .= $individualVar[0] . "= '" . urldecode( $individualVar[1] ) . "', ";
			}
		}

		$PARAMETERS1 = substr( $stringVarsUrl, 0, - 2 );

		###HOT FIX - TO GET KEYWORDS FROM REFERENCE
		$PARAMETERS = str_replace( '&', ', -', $sessionData->HTTP_REFERER );
		$PARAMETERS = str_replace( '?', ', -', $PARAMETERS );
		$PARAMETERS = str_replace( '/', ', -', $PARAMETERS );
		$importantVars = array( '-q', '-utm_source', '-utm_medium', '-utm_campaign' );
		$stringVarsUrl = '';
		$varsInUrl = explode( ',', $PARAMETERS );
		foreach ( $varsInUrl as $val )
		{
			$individualVar = explode( '=', $val );
			if ( in_array( trim( $individualVar[0] ), $importantVars ) )
			{
				$stringVarsUrl .= $individualVar[0] . "= '" . urldecode( $individualVar[1] ) . "', ";
			}
		}
		$PARAMETERS2 = substr( $stringVarsUrl, 0, - 2 );

		$urlDeDondeProviene = str_replace( "http://", "", $sessionData->HTTP_REFERER );
		$urlDeDondeProviene = str_replace( "https://", "", $urlDeDondeProviene );
		$urlDeDondeProviene = substr( $urlDeDondeProviene, 0, strrpos( $urlDeDondeProviene, "/" ) );

		$PARAMETERS = $PARAMETERS1 . " " . $PARAMETERS2 . " url=" . $urlDeDondeProviene;  // ." S=" . $sessionData->SESSION_ID;

		return $PARAMETERS;
	}

	remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
	remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

	add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
	add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

	function my_theme_wrapper_start() {
		echo '<section id="main">';
	}

	function my_theme_wrapper_end() {
		echo '</section>';
	}

	add_action( 'after_setup_theme', 'woocommerce_support' );
	function woocommerce_support() {
		add_theme_support( 'woocommerce' );

	}
?>
