﻿<?php define("sugar_location",'http://sugar.gastronomicainternacional.com/soap.php?wsdl');
define("sugar_uri",'http://sugar.gastronomicainternacional.com/');

$contriesByLada=array(
	'52'=>'Mexico',
	'1'=>'United States',
	'34'=>'España',
	'58'=>'Venezuela',
	'51'=>'Peru',
	'56'=>'Colombia',
	'54'=>'Argentina'
);


// ***** FORMULARIO CONTACTO RÁPIDO
if(isset($_POST['submit_fast'])){
	#VIENE_DE @author: Nathaly
	$vieneDe = getVieneDe();
	#END VIENE_DE
	
  if(!defined('sugarEntry')) define('sugarEntry', true);
	
	//host connection
	$options = array(
		"location" => sugar_location,
		"uri" => sugar_uri,
		"trace" => 1
	);

	//user connection
	$user_auth = array(
	"user_name" => 'admin',
	"password" => MD5('martin2013'),
	"version" => '.01'
	);

	$client = new SoapClient(NULL,$options);

	//login sugar
	$client_res = $client->login($user_auth,"admin");

	$session_id = $client_res->id;

	# START VALIDAR DUPLICADOS @author: Nathaly
		$emailLead = trim($_POST['field_email']);
		#	$emailLead = "enat87@gmail.com";
		
		$getDuplicados = $client->get_entry_list(
			$session_id, 
			'Opportunities' ,
			' trim(email_c) like "'.$emailLead.'"', #query
			'email_c', #order by
			0, #offset
			array('id', 'name', 'email_c') #,  #select_fields
			# array('email_c', 'name', 'id') # link_name_to_fields_array()
			);

		$duplicados = $getDuplicados->entry_list;

		if (count($duplicados) > 0){
			$_POST['field_name'] = "- D - ". $_POST['field_name']  ;
		}

	# END VALIDAR DUPLICADOS

	// create a new contact record, assigned to this account, and grab the contact ID
	$response = $client->set_entry($session_id, 'Opportunities', array(
		array("name" => 'viene_de_c',"value" => ($vieneDe) ),  #@author: Nathaly
		array("name" => 'name',"value" => (isset($_POST['field_name'])? $_POST['field_name']: '') ),
		array("name" => 'email_c',"value" => (isset($_POST['field_email'])? $_POST['field_email']: '') ),
		array("name" => ((isset($_POST['field_phon_type']) && $_POST['field_phon_type']=='mobile')? 'telfalternativo_c': 'telf_c')
				,"value" => (isset($_POST['field_telefono'])? $_POST['field_telefono']: '') ),
		array("name" => 'lada_c',"value" => (isset($_POST['field_lada'])? $_POST['field_lada']: '') ),
		array("name" => 'cursosinteres_c',"value" => (isset($_POST['field_area'])? $_POST['field_area']: '') ),
		array("name" => 'comentario_c',"value" => (isset($_POST['field_questions'])? $_POST['field_questions']: '') ),
		array("name" => 'pais_c',"value" => (isset($_POST['field_lada'])? (isset($contriesByLada[(int)$_POST['field_lada']])? $contriesByLada[(int)$_POST['field_lada']]: ''): '') )
	));
	echo $response->error->number==0?1:0;
	
	// SEND MAIL
	
	function sendContactMail($to, $subject, $fileContent, $vars=array()){
		//$sender_mail = 'info@gastronomicainternacional.com';
		//$sender_name = 'GastronONOmica Internacional';
		$headers=array(
		//	"From: ".$sender_name." <".$sender_mail.">\r\n",
			//"Reply-To: ". strip_tags($_POST['req-email']) . "\r\n",
			//"CC: susan@example.com\r\n",
			"MIME-Version: 1.0\r\n",
			"Content-Type: text/html; charset=UTF-8\r\n",
		);
		$content=file_get_contents($fileContent);
		foreach($vars as $var) $content=str_replace(array_keys($vars), $vars, $content);
		wp_mail( $to, $subject, $content, implode('',$headers) );
		// mail($to, $subject, $content, implode('',$headers));
		
		file_put_contents('mail.log', 'to:'.$to."\tSubject:".$subject."\tContent:\n".$content."\n" , FILE_APPEND | LOCK_EX);
		//mail($to, $subject, $content, implode('',$headers));
	}
		

	$sendmail=true;
	if($sendmail){
		// mail Admin
		$to = array('jluis@gastronomicainternacional.com');
		$vars=array(
			'<%domain%>' => $_SERVER['SERVER_NAME'],
			'<%url%>' => get_bloginfo('template_url'),
			'<%name%>' => (isset($_POST['field_name'])? $_POST['field_name']: ''),
			'<%email%>' => (isset($_POST['field_email'])? $_POST['field_email']: ''),
			'<%mobile%>' => ((isset($_POST['field_phon_type']) && $_POST['field_phon_type']=='mobile' && isset($_POST['field_telefono']))? $_POST['field_telefono']: ''),
			'<%phone%>' => ((isset($_POST['field_phon_type']) && $_POST['field_phon_type']!='mobile' && isset($_POST['field_telefono']))? $_POST['field_telefono']: ''),
			'<%lada%>' => (isset($_POST['field_lada'])? $_POST['field_lada']: ''),
			'<%area%>' => (isset($_POST['field_area'])? $_POST['field_area']: ''),
			'<%description%>' => (isset($_POST['field_questions'])? $_POST['field_questions']: ''),
			'<%contry%>' => (isset($_POST['field_lada'])? (isset($contriesByLada[$_POST['field_lada']])? $contriesByLada[$_POST['field_lada']]: ''): '')
		);
		sendContactMail( $to, 'Registro recibido - Gastronómica Internacional', get_bloginfo('template_url').'/mailers/contact-forAdmin.php', $vars );
		
		// mail User
		if(isset($_POST['field_email']))
			sendContactMail( $_POST['field_email'], 'Gracias por contactarnos.', get_bloginfo('template_url').'/mailers/contact-forUser.php', $vars );
	}
	
}