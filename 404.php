<?php

get_header();
?>
	<div id="primary" class="content-area">
		<div id="pagecontent" class="site-content" role="main" style="text-align:left; min-height:200px;">


			<!--<div class="p404" style="color:#555; font-size:8rem; height:88px; padding-top:70px;">=(</div>-->
			<p style="color:#555; font-size:1.2rem; ">Lo sentimos, la página que estás buscando no existe<br>o ha sido movida a otra dirección.</p>
			<ul>
			<li>Revisa que la dirección URL está bien escrita.</li>
			<li>Visita cualquier otro contenido desde el menú de navegación.</li>
			<li>Si quieres volver al inicio del sitio puedes dar clic <a href="http://gastronomicainternacional.com">aquí</a></li>
			</ul>

		</div>
	</div>
<?php
get_footer();