<?php
/*
Template Name: Ajax
*/
header('Content-Type:application/json');
echo json_encode( get_page( get_the_ID() ) );
