<?php
/*
Template Name: Blog
*/
get_header();
	require( ((wp_is_mobile() && ($_SESSION["m_desktop"]!=true)) ? 'mobile': 'web').'/page.articulosculinarios.php' );
get_footer();

/*
get_header();

// carga las opciones de la plantilla
$options=get_option('pa_opciones');

	// es categoría o es un artículo
	$isCategory=get_the_category();
	$isPost=is_single();
	$isAuthor=is_author();
	
	// carga la categoría
	$current_categories = get_the_category();
	$category_slug = get_queried_object()->slug;
	$category=current($current_categories);
		foreach($current_categories as $cat)
			if($category_slug==$cat->slug) $category=$cat;

	// carga el artículo
	$post = get_post();
	$post_slug = $post->post_name;
	
	//carga el autor
	$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
	$author = $author->data;
	
	// urlCarpeta
	$urlCaerpeta=$isAuthor?'/blog-author/'.$author->user_nicename:'/blog/';
	
// variables principales
$description = $isPost?
	'Categoría: '.($isCategory? $category->name: '')
	: 'Conoce lo último en tendencias en nuestros artículos.'; 
?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/web/css/articulosculinarios.css">

	<div id="primary" class="content-area">
		<div id="pagecontent" class="site-content" role="main">
			<div class="breadcrumbs"><a href="/">HOME</a><span>&gt;</span>
				<a href="/">Home</a><span>&gt;</span>
				<a href="/blog/todos">Artículos Culinarios</a>
				<?php if($isAuthor){ ?>
					<span>&gt;</span><a href="<?php echo $urlCaerpeta; ?>" class="active"><?php echo $author->display_name; ?></a>
				<?php }else if($isCategory){ ?>
					<span>&gt;</span><a href="<?php echo $urlCaerpeta.$category_slug; ?>" class="active"><?php echo $category->name; ?></a>
				<?php } ?>
				<?php if($isPost){ ?>
					<span>&gt;</span><a href="/<?php echo $post_slug; ?>" class="active"><?php echo $post->post_title; ?></a>
				<?php } ?>
			</div>
			
			<div class="articulosculinarios">
				<div class="nivel2">
					<div class="articulosculinarios_title"><span>Artículos</span><br>Culinarios</div>
					<div class="articulosculinarios_content"><?php echo $description; ?></div>
					<div class="content">
<?php
	if($isPost){
	// *********** ARTICULO 

		// 
		$post= $posts[0];
		
			$cats=wp_get_post_categories( $post->ID );
		
		$image=get_the_post_thumbnail( $post->ID );
		$date = new DateTime($post->post_date); $date= $date->format('d / m / Y');
		$author = get_the_author_meta( 'nickname', $post->post_author );
		$title = $post->post_title;
		$extract = $post->post_excerpt;
		$content= $post->post_content;
		$categories = get_the_category($post->ID);
		$link= $post->post_name; 
?>			<div class="article_title"><?php echo $title; ?></div>
			<div class="article_date"><?php echo $date; ?></div>
			<div class="article_author"><span>Author: </span><?php echo $author; ?></div>
			<div class="article_extract"><?php echo $extract; ?></div>
			
			<div class="article_content">
				<!--div class="article_img"><?php echo $image==''? '<span class="nada"></span>': $image; ?></div-->
			<?php echo $content; ?></div>
		<?php
		// ----------- FIN ARTICULO		
		
		
	}else {
	// *********** CATEGORIA

		$post_per_page = 4;
		$offset=isset($_GET['offset'])? (int)$_GET['offset']: 0;
		
		$query_array=array(
			'posts_per_page'   => $post_per_page,
			'offset'           => $offset*$post_per_page,
			'category_name'    => $category_slug,
			'orderby'          => 'post_date',
			'order'            => 'DESC',
			'post_type'			=> 'post',
			'post_status'		=> 'publish'
		);
		if($isAuthor){ $query_array['author']=$author->ID; ?><!-- ASOC:[<?php echo $author->ID;	?>]--><?php }
		$query = new WP_Query($query_array);						
		$posts_array=$query->posts;
		$max_num_post = $query->max_num_pages; 
?>		
		<ul class="list_post">
<?php	foreach($posts_array as $post){
			$image=get_the_post_thumbnail( $post->ID ); $image= $image!=''? $image: '<span class="noImage"></span>';
			$date = new DateTime($post->post_date); $date= $date->format('d / m / Y');
			$author = get_the_author_meta( 'nickname', $post->post_author );
			$title = $post->post_title;
			$extract = $post->post_excerpt;
			$content= $post->post_content;
			$categories = get_the_category($post->ID);
			$link= '/'.$post->post_name; ?>
			<li class="article">
				<div class="img"><?php echo $image==''? '<span class="nada"></span>': $image; ?></div>
				<div class="match">
					<div class="date"><?php echo $date; ?></div>
					<div class="author"><span class="label">Autor:</span><?php echo $author; ?></div>
					<div class="title"><?php echo $title; ?></div>
					<div class="extract"><?php echo $extract; ?></div>
					<a class="link" href="<?php echo $link; ?>">Ver más</a>
					<div class="clearBoth"></div>
				</div>
				<div class="clearBoth"></div>
			</li>
			<?php
		}
		if(count($posts_array)==0){ ?>No se encontraron artículos en está sección.<?php }
		?>
		</ul>
<?php 
	$category_url=$isCategory? $urlCaerpeta.$category_slug: '/blog/todos';
	$category_and_offset_prev= $offset==0? '#': $category_url.'?offset='.($offset-1);
	$_prev_class= $offset==0? 'disabled': '';
	$category_and_offset_next= $max_num_post-1==$offset? '#': $category_url.'?offset='.($offset+1);
	$_next_class= $max_num_post-1==$offset? 'disabled': '';
?>		<div class="paginador">
			<div class="label">Pagina <?php echo $offset+1; ?> de <?php echo $max_num_post; ?></div>
			<div class="nav">
				<a class="prev <?php echo $_prev_class; ?>" href="<?php echo $category_and_offset_prev ?>" >Anterior</a>
				<a class="next <?php echo $_next_class; ?>" href="<?php echo $category_and_offset_next ?>" >Siguiente</a>
				<div class="paginator">
				<?php 
				$margin=3; $pos_a=$margin; $pos_b=$offset-$margin+1; $pos_c=$offset+$margin; $pos_d=$max_num_post-$margin;
				for($i=0; $i<$max_num_post; $i++){
					if($pos_a<$pos_b and $i>=$pos_a and $i<$pos_b ){ $i=$pos_b; ?><span>...</span><?php }
					if($pos_c<$pos_d and $i>=$pos_c and $i<$pos_d ){ $i=$pos_d; ?><span>...</span><?php }
					if($i==$offset){
						?><b><?php echo $i+1; ?></b><?php
					}else{
						?><a href="<?php echo $category_url.'?offset='.$i; ?>" ><?php echo $i+1; ?></a><?php
					}
				} ?>
				</div>
			</div>
		</div>
<?php 

		// ----------- FIN CATEGORIA

		if(!isset($cats)) $cats=array();

	}
?>					</div>
					<ul class="submenu">
						<div class="title">Ver artículos de:</div>
<?php					$categories=get_categories(array(
							'type'                     => 'post',
							'child_of'                 => 0,
							'parent'                   => '',
							'orderby'                  => 'ID',
							'order'                    => 'ASC',
							'hide_empty'               => 0,
							'hierarchical'             => 1,
							'exclude'                  => '',
							'include'                  => '',
							'number'                   => '',
							'taxonomy'                 => 'category',
							'pad_counts'               => false 

						));
						foreach($categories as $cat){
							$class = $category_slug==$cat->slug? 'active' :'' ;
							$class.=' '.(in_array($cat->term_id, $cats)? 'in ': '');	?>
							<li class="<?php echo $class; ?>">
								<a href="<?php echo  '/blog/'.$cat->slug; ?>"><?php echo $cat->name; ?></a>
							</li><?php
						}
?>
					</ul>
				</div>
			</div>
		</div>
	</div>
<?php	
get_footer();	
?>*/