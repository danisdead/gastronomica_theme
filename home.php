<?php
/**
 *
 * @link http://lead2action.com
 *
 * @package WordPress
 * @subpackage Gastronomica_Internacional_2014
 * @since Gastronomica Internacional 2014 1.0
 */

if(isset($_POST['submit_fast'])){
	include('action.php');
	exit();
}

get_header();
	require( ((wp_is_mobile() && ($_SESSION["m_desktop"]!=true)) ? 'mobile': 'web').'/home.php' );
get_footer();
