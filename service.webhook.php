<?php
	/*
		Template Name: webhook
	*/
	/**
	 * Created by PhpStorm.
	 * User: Daniel
	 * Date: 9/16/15
	 * Time: 10:17 AM
	 */

	function sendDebugMail( $debugTitle, $debugInfo ) {
		mail( "daniel@kokonutstudio.com", $debugTitle, print_r( $debugInfo, true ) );
	}

	$myemail = 'daniel@kokonutstudio.com';//<-----Put Your email address here.
	$response = array();

	if ( isset( $_POST[ 'data_json' ] ) ) {
		$form_data = json_decode( stripslashes( $_POST[ 'data_json' ] ) );

//		sendDebugMail('Form data', $form_data);

		$nombre = $form_data->first_name[ 0 ];
		$apellido = $form_data->last_name[ 0 ];
		$email = $form_data->email[ 0 ];
		$tel = $form_data->phone_number[ 0 ];
		$tipo_tel = $form_data->tipo_telefono[ 0 ];
		$area = $form_data->area_de_interes[ 0 ];
		$objeto = $form_data->objeto_de_estudio[ 0 ];

		$vieneDe = "landingTest";
		$vieneDe = $form_data->viene_de[ 0 ];

################### CREANDO EL LEAD EN SUGAR ######################################
		define("sugar_location",'http://sugar.gastronomicainternacional.com/soap.php');
		define("sugar_uri",'http://sugar.gastronomicainternacional.com/');
		if(!defined('sugarEntry')) define('sugarEntry', true);
		//host connection
		$options = array(
			"location" => sugar_location,
			"uri" => sugar_uri,
			"trace" => 1
		);

		//user connection
		$user_auth = array(
			"user_name" => 'admin',
			"password" => MD5('martin2013'),
			"version" => '.01'
		);

		$client = new SoapClient(NULL,$options);

		//login sugar
		$client_res = $client->login($user_auth,"admin");
		$session_id = $client_res->id;
		# START VALIDAR DUPLICADOS @author: Nathaly
		$emailLead = trim($email);
		#	$emailLead = "enat87@gmail.com";
		$getDuplicados = $client->get_entry_list(
			$session_id,
			'Opportunities',
			'trim(email_c) like "'.$emailLead.'"',
			'email_c',
			0,
			array(
				'id',
				'name',
				'email_c'
			)
		);

		$duplicados = $getDuplicados->entry_list;
		if (count($duplicados) > 0){
			$nombre= "- D - ". $nombre ;
		}
		# END VALIDAR DUPLICADOS
		// create a new contact record, assigned to this account, and grab the contact ID

		$response = $client->set_entry($session_id, 'Opportunities', array(
			array("name" => 'name',"value" => $nombre . ' ' . $apellido),
			array("name" => 'nombre_c',"value" => $nombre),
			array("name" => 'apellidos_c',"value" => $apellido),
			array("name" => 'email_c',"value" => $email),
			array("name" => ( ($tipo_tel=='Celular') ? 'telfalternativo_c' : 'telf_c' ),
				  "value" => ( $tel )
			),
			// array("name" => 'telf_c',"value" => $tel),
			array("name" => 'cursosinteres_c',"value" => $area),
			array("name" => 'objetoestudio_c',"value" => $objeto),
			array("name" => 'viene_de_c',"value" => $vieneDe)
		));

		$leadCreado = $response->error->number==0?1:0;
//		sendDebugMail('step leadCreado', $leadCreado);
		// SEND MAIL

		if ( $leadCreado == 1 ) {
//			sendDebugMail('step leadCreado 2', $leadCreado);
			$to = $myemail;
			$email_subject = "Registro recibido - Gastronómica Internacional";
//			sendDebugMail('step semi semi final', $myemail . " " . $email_subject);

			$email_body = "Registro Recibido. " .
				"\n Nombre: " . $nombre .
				"\n Apellido: " . $apellido .
				"\n Email: " . $email .
				"\n Teléfono: " . $tel .
				"\n Tipo de Teléfono: " . $tipo_tel .
				"\n Área de Interés: " . $area .
				"\n Viene de: " . $vieneDe;

//			sendDebugMail('step final', $email_body);

			$headers = "From: " . $myemail ."\n";
			$headers .= "Reply-To: " . $email ;


//			mail( $to, $email_subject, $email_body, $headers );
		} else{
//			sendDebugMail('step 13', '');
		}

###################END CREACION DE LEAD EN SUGAR #################################

	}
	else {
		$response[ 'status' ] = 'Error';
		$response[ 'msg' ] = 'Hubo un error en la comunicación.';
	}
