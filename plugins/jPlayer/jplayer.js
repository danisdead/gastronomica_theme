$(document).ready(function(){
	$('[data-video]').each(function(i){
		var me=$(this);
		me.html(
'<div id="jp_container_'+i+'" class="jp-video " role="application" aria-label="media player">'+
'  <div class="jp-type-single">'+
'    <div id="jquery_jplayer_'+i+'" class="jp-jplayer"></div>'+
'    <div class="jp-gui">'+
'      <div class="jp-video-play">'+
'        <button class="jp-video-play-icon" role="button" tabindex="0">play</button>'+
'      </div>'+
'      <div class="jp-interface">'+
'        <div class="jp-progress">'+
 '         <div class="jp-seek-bar">'+
 '           <div class="jp-play-bar"></div>'+
 '         </div>'+
 '       </div>'+
 '       <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>'+
 '       <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>'+
 '       <div class="jp-details">'+
 '         <div class="jp-title" aria-label="title">&nbsp;</div>'+
 '       </div>'+
 '       <div class="jp-controls-holder">'+
 '         <div class="jp-volume-controls">'+
 '           <button class="jp-mute" role="button" tabindex="0">mute</button>'+
 '           <button class="jp-volume-max" role="button" tabindex="0">max volume</button>'+
 '           <div class="jp-volume-bar">'+
 '             <div class="jp-volume-bar-value"></div>'+
 '           </div>'+
 '         </div>'+
 '         <div class="jp-controls">'+
 '           <button class="jp-play" role="button" tabindex="0">play</button>'+
 '           <button class="jp-stop" role="button" tabindex="0">stop</button>'+
 '         </div>'+
 '         <div class="jp-toggles">'+
 '           <button class="jp-repeat" role="button" tabindex="0">repeat</button>'+
 '           <button class="jp-full-screen" role="button" tabindex="0">full screen</button>'+
 '         </div>'+
 '       </div>'+
 '     </div>'+
 '   </div>'+
 '   <div class="jp-no-solution">'+
 '     <span>Update Required</span>'+
 '     To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.'+
 '   </div>'+
 ' </div>'+
 '</div>'
		);
		
		var mePalyer=$('.jp-jplayer',me);
		
		mePalyer.jPlayer({
			errorAlerts:true, warningAlerts:true, consoleAlerts:true,
			ready: function () {
			  $(this).jPlayer("setMedia", {
				title: me.attr('title'),
				m4v: me.attr('data-video'),
				poster: me.attr('data-poster')
			  });
			},
			cssSelectorAncestor: "#jp_container_"+i,
			swfPath: "js/jPlayer",
			supplied: "m4v, ogv",
			useStateClassSkin: true,
			autoBlur: false,
			smoothPlayBar: true,
			keyEnabled: true,
			remainingDuration: true,
			toggleDuration: true,
			size: {
				width: (me.attr('width') || 640),
				height: (me.attr('height') || 360)
			}
		});
	  
		var seconds_satarts;
		
		mePalyer.bind($.jPlayer.event.play, function(event) {
				 var playerTime = Math.round(event.jPlayer.status.currentTime);
				 seconds_satarts = playerTime;
		});
		//listener for a pause click
		mePalyer.bind($.jPlayer.event.pause, function(event) {
				 var playerTime = Math.round(event.jPlayer.status.currentTime);
				 var mediaName = me.attr('title'); //event.jPlayer.status.src;
				 if(playerTime<event.jPlayer.status.duration){
				ga('send', 'event', {eventCategory: 'video_range', eventAction: mediaName, eventLabel:playerTime-seconds_satarts});
				}
		});
		//listening for the user dragging the seek bar
		mePalyer.bind($.jPlayer.event.seeking, function(event) {
				 var playerTime = Math.round(event.jPlayer.status.currentTime);
				 var mediaName = me.attr('title'); //event.jPlayer.status.src;
				 ga('send', 'event', {eventCategory: 'video_range', eventAction: mediaName, eventLabel:playerTime-seconds_satarts});
		});
		//listening for when the user has stopped dragging the seek bar
		mePalyer.bind($.jPlayer.event.seeked, function(event) {
				 var playerTime = Math.round(event.jPlayer.status.currentTime);
				 seconds_satarts = playerTime;
		});
		//listening for an end ie file completion
		mePalyer.bind($.jPlayer.event.ended, function(event) {
				 var playerTime = Math.round(event.jPlayer.status.duration);
				 var mediaName = me.attr('title'); //event.jPlayer.status.src;
				 ga('send', 'event', {eventCategory: 'video_range', eventAction: mediaName, eventLabel:playerTime-seconds_satarts});
		});
	});
});