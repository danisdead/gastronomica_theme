// **** Oferta educativa ****

// **** Nivel 2 - Sub menu lateral

$(function(){
	var menu=$('#level2_linkajax')
	var content=$('#ajaxable_content');
	var links=menu.find('a');
	links.click(function(){
		var link=$(this);
		$.getJSON(link.attr('href'),function(data){
			links.removeClass('active');
			link.addClass('active');
			content.empty();
			content.html(data.post_content);
		});
		return false;
	});
	links.first().click();
})