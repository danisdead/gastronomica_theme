var menu_width = 0;


$(function(){
	menu_width = $(window).width()-61;
	
	function theResize(){
		menu_width = $(window).width()-61;
		var menu_height = $(window).height()-80;
		$("#header .main_menu").css({width:60}).attr('data-expand','false');
		$("#header .main_menu ul").css({left:0,width:menu_width,display:"none", 'max-height':menu_height});
		$("#header .main_menu > ul").css({left:-menu_width,display:"block"});
		$("#mb_menu_btn").removeClass("active");
		$("body").css({"overflow":"auto"});
	}
	$(window).resize(theResize);
	theResize();
	
	$("#mb_menu_btn").click(function(){
		var m = $("#header .main_menu");
		if(m.attr("data-expand")!="true"){
			$("#mb_menu_btn").addClass("active");
			m.attr("data-expand","true");
			m.stop().css({width: 100,height:'auto'});
			var h = m.height();
			m.stop().css({width: 60,height:60}).animate({width:menu_width,height:h},function(){ m.css({height:'auto'}); });
			var o = $("#header .head_contact")
			if(o.attr("data-expand")=="true") $("#mb_contact_btn").trigger("click");
			$("#header .main_menu ul").css({opacity:1,"overflow-x":"visible", "overflow-y":"visible"});
			$("#header .main_menu > ul").css({opacity:0,"overflow-x":"visible", "overflow-y":"auto"}).animate({left:0,opacity:1});
			$("body").css({"overflow":"hidden"});
		}else{
			$("#mb_menu_btn").removeClass("active");
			m.attr("data-expand","false");
			m.stop().animate({width:60,height:60},function(){
				$("#header .main_menu > ul > li > ul").hide();
			});
			$("#header .main_menu ul").animate({left:-menu_width, opacity:0});
			$("body").css({"overflow":"auto"});
		}
	});
	//$("#header .main_menu .noChildren ul").remove();
	/*$("#header .main_menu > ul > li > ul").before("<div class='dropdown'></div>");*/
	$("#header .main_menu ul").css({left:0,width:menu_width, "overflow-x":"visible", "overflow-y":"visible"});
	$("#header .main_menu > ul").css({left:-menu_width});
	$("#header .main_menu .dropdown").click(function(e){
		e.stopPropagation();
		var that = $(this)
		$("#header .main_menu ul").css({"overflow-x":"visible", "overflow-y":"visible"});
		that.parent().animate({left:-menu_width,opacity:0},200,function(){
			$(window).scrollTop(0);
			that.parent().css({opacity:1});
			that.children("ul").css({display:"block",opacity:0,left:menu_width,"overflow-x":"visible", "overflow-y":"auto"}).animate({left:0,opacity:1},200);
		});
		/*var m = $(this).find("ul");
		var m = $(this).children("ul");
		var v = m.is(':visible')
		$("#header .main_menu > ul > li > ul").hide();
		if(!v) m.show();*/
	});
	$("#header .main_menu .anchor").click(function(e){
		e.stopPropagation();
		$(this).children("a").eq(0).trigger("click");
	});
	$("#header .main_menu .back").click(function(e){
		e.stopPropagation();
		var that = $(this);
		that.parent().animate({left:"100%",opacity:0},200,function(){
			$("#header .main_menu ul").css({"overflow-x":"visible", "overflow-y":"visible"});
			that.parent().css({opacity:1});
			that.parent().parent().parent().css({opacity:0}).animate({left:0,opacity:1},200,function(){
				that.parent().parent().parent().css({"overflow-x":"visible", "overflow-y":"auto"});
				$('ul.level_1:visible, ul.level_0:visible').scrollTop(0);
			});
		})
	});

	
	
	$("#header .head_logo").click(function(){
		location.href = "/";
	});
	$("#mb_contact_btn").click(function(){
		location.href = "/contacto/";
		/*var m = $("#header .head_contact");
		if(m.attr("data-expand")!="true"){
			m.attr("data-expand","true");
			m.stop().css({width: 100,height:'auto'});
			var h = m.height();
			m.stop().css({width: 60,height:60}).animate({width:'80%',height:h},function(){ m.css({height:'auto'}); });
			var o = $("#header .main_menu")
			if(o.attr("data-expand")=="true") $("#mb_menu_btn").trigger("click");
		}else{
			m.attr("data-expand","false");
			m.stop().animate({width:60,height:60},function(){
				$("#header .main_menu > ul > li > ul").hide();
			});
		}*/
	});


	/*var moblile_width_borderline=700;
	var _window=$(window).add(document);
	$('#main_menu').click(function(event){
		// cacha el evento, el target y su padre
		var event = event || window.event;
		var me= $(event.target || event.srcElement);
		var parent= me.parent(), siblingsUL;
		// si es una liga
		if(me.is('a') || parent.is('a')){
			me=me.is('a')? me: parent;
			siblingsUL=me.siblings('ul');
			// sii tiene hijos
			if(siblingsUL.find('a').length>0){
				// si el ancho de la p�gina es menos de 700px de ancho
				if(_window.width()< moblile_width_borderline){ siblingsUL.toggle(); }
				// si el ancho de la p�gina es mayor de 700px de ancho
				else{ }
				return false;
			}
		}
	});
	$('#main_menu').find('li.level_0')
	.mouseenter(function(){
		if(_window.width()>=moblile_width_borderline){
			$(this).children('ul').show();
		}
	})
	.mouseleave(function(){
		if(_window.width()>=moblile_width_borderline){
			$(this).children('ul').hide();
		}
	});*/
	
	$(window).on('beforeunload', function() {
		$(window).scrollTop(0);
	});
	
	
	var tabtrs = $(".tableinblock tr");
	for(var i=0;i<tabtrs.length;i++){
		var tr = tabtrs.eq(i);
		var tds = tr.children("td").detach();
		tr.append("<td style='width:100%;'></td>");
		var ntd = tr.children("td").eq(0);
		for(var j=0;j<tds.length;j++){
			ntd.append("<div></div>");
			var insert = tds.eq(j).html();
			var last = ntd.children("div").last();
			last.html(insert);
		}
	}
});
	
