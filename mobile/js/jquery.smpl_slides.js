jQuery.fn.smpl_hslide=function(fn_in,fn_out,fn_change,fn_start){
	var me=jQuery(this), i, _current=0, _prev=0;
	var _slides=me.children('div.slide'), slides=[]; console.log(_slides.length)
		for(i=0; i<_slides.length; i++) slides[i]=jQuery(_slides[i]);
		
	
	// *** crea el navegador ***
	var nav=jQuery('<ul class="slide_nav"></ul>');
	// agrega las ligas
	var prev=jQuery('<li class="prev"><a>&lt;</a></li>'); nav.append(prev);
	var links=[];
	for(i=0; i<slides.length; i++){
		links[i]=jQuery('<li class="link link_'+i+'"><a>'+i+'</a></li>');
		nav.append(links[i]);
	}
	var next=jQuery('<li class="next"><a>&gt;</a></li>'); nav.append(next);
	// agrega el navegador al slide y activa el primero, esconde los slides
	me.prepend(nav);
	prev.addClass('disable');
	links[0].addClass('active');
	slides[0].addClass('active').siblings('div.slide').hide();
	
	// funcion previa a la animación
	me.isanimating=false;
	var animate=function(){
		// si son el mismo slide termina
		if(_current==_prev) return false;
		// activa y desactiva las flechas navegadoras
		if(_current==0 && !prev.hasClass('disable')){ prev.addClass('disable'); }
		else if(_current>0 && prev.hasClass('disable')){ prev.removeClass('disable'); }
		if(_current==links.length-1 && !next.hasClass('disable')){ next.addClass('disable'); }
		else if(_current<links.length-1 && next.hasClass('disable')){ next.removeClass('disable'); }
		// activa y desactiva los links navegadores
		links[_current].addClass('active').siblings('.link').removeClass('active');
		slides[_current].addClass('active').siblings('div.slide').removeClass('active');
		// ejecuta la función de animación
		me.isanimating=true;
		fn_in(slides[_prev], slides[_current], fn_out, me);
	}
	
	me.backedMove=false;
	me.executeBackedMoves=function(){ if(me.backedMove!=false) me.backedMove.click(); me.backedMove=false; };
	
	me.update=function(){ me.isanimating=false; me.executeBackedMoves(); }
	
	// *** funcionalid links navegadores ****
	for(i=0; i<links.length; i++)
		links[i].click(function(){
			if(me.isanimating){ me.backedMove=$(this); return false; }
			_prev=_current
			_current=jQuery(this).siblings('.link').add(this).index(this);
			animate();
			return false;
		});
	
	// *** funcionalid flechas navegadores ****
	prev.click(function(){
		if(me.isanimating){ me.backedMove=$(this); return false; }
		_prev=_current;
		_current--; if(_current<0) _current=slides.length-1;
		animate();
		return false;
	});
	next.click(function(){
		if(me.isanimating){ me.backedMove=$(this); return false; }
		_prev=_current;
		_current++; if(_current>=slides.length) _current=0;
		animate();
		return false;
	});
	
	// **** funcionalidad drag ****
	var _dragin=false;
	var mousedown=function(event){
		event = event.originalEvent; 
		event=event.touches? event.touches[0]: event;
		_dragin={x: event.pageX, y:event.pageY, time:(new Date()).getTime()};
	}
	var mouseup=function(event){
		if(_dragin!=false){
			event = event.originalEvent;
			event=event.touches? event.touches[0]: event; //var a=[]; for(var i in event) a.push(i+'='+event); alert(a); alert(event.pageX[0]);/*
			var dragLen=event.pageX-_dragin.x;
			var time=(new Date()).getTime()-_dragin.time;
			_dragin=false;
			if(Math.abs(dragLen)>10 && time<800){
				if(!me.isanimating)
				{ dragLen>0? prev.click(): next.click(); }
			}
				
		}
	}
	me.mousedown(mousedown);
	jQuery(document).mouseup(mouseup);
			jQuery(document).on('touchmove',function(e){ mouseup(e); });
			me.on('touchstart',function(e){ mousedown(e); });
		
			
	// ****************************************
	// ++++ FUNCIONES ANIMACION DEFAUKT +++++++
	fn_in=(fn_in instanceof Function)?fn_in:
		function(slide_out, slide_in, fn_out, origin){
			jQuery(slide_out).animate({opacity:0},200,function(){
				jQuery(this).css({display:'none'});
				fn_out(slide_in, origin); //IMPORTANTISIMO: al finalizar la animación de esconderlo, llama la entrada del otro
			});
		};
	fn_out=(fn_out instanceof Function)?fn_out:
		function(slide_in, origin){
			jQuery(slide_in).css({display:'block', opacity:0});
			jQuery(slide_in).animate({opacity:1},200,function(){
				origin.update(); //IMPORTANTISIMO: reactiva los botones de navegación
				fn_change(slide_in); // al finalizar la animación de esconderlo, llama la entrada del otro
			});
		};
	fn_change=(fn_change instanceof Function)?fn_change:new Function();
	fn_start=(fn_start instanceof Function)?fn_start:new Function();
	fn_start(me);
}

jQuery(function(){
	$('.smpl_hslide').each(function(){
		var dataClass=$(this).attr('class');
		switch(dataClass){
			default: $(this).smpl_hslide(); break;
			case 'smpl_hslide slideImage':
				var fn_in=function(){
					},
					fn_out=function(){
					},
					fn_change=function(){
					},
					fn_start=function(){
					};
				$(this).smpl_hslide();
				break;
		}
	});
});