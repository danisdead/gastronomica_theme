<!--div id='home'>
	<div class='slider1'>
		<div class='slide uno'>
			<img class='pic' src='http://placehold.it/800x460/ff0000/000000&text=UNO' />
			<p><strong>ASESORÍA</strong>PROFESIONAL A UN CLIC DE DISTANCIA</p>
			<img class='button' src='http://placehold.it/662x88/ff0000/000000&text=CONOCE%20M%C3%81S%20%3E' />
		</div>
		<div class='slide dos'>
			<img class='pic' src='http://placehold.it/800x460/0000ff/000000&text=DOS' />
			<p><strong>HA DE TU COCINA</strong>TU TALLER GASTRONÓMICO</p>
			<img class='button' src='http://placehold.it/662x88/00ff00/000000&text=DIPLOMADO%20EN%20COCINA%20%3E' />
		</div>
		<div class='slide tres'>
			<img class='pic' src='http://placehold.it/800x460/00ff00/000000&text=TRES' />
			<p><strong>PRESTIGIO Y VALIDEZ</strong>INTERNACIONAL</p>
			<img class='button' src='http://placehold.it/662x88/0000ff/000000&text=CONOCE%20M%C3%81S%20%3E' />
		</div>
		<div class='control'>
			<div class='bullet'></div>
			<div class='bullet'></div>
			<div class='bullet'></div>
		</div>
	</div>
	<div class='tabs1'>
		<h3>Una formación<br/><strong>gastronómica integral.</strong></h3>
		<div class='tab'>
			<img src='http://placehold.it/800x250/ff0000/000000&text=atencion' />
			<p>ATENCIÓN PERSONALIZADA<p>
		</div>
		<div class='tab'>
			<img src='http://placehold.it/800x250/0000ff/000000&text=instructores' />
			<p>INSTRUCTORES<p>
		</div>
		<div class='tab'>
			<img src='http://placehold.it/800x250/00ff00/000000&text=certificados' />
			<p>CERTIFICADOS DE ESTUDIO<p>
		</div>
		<div class='tab'>
			<img src='http://placehold.it/800x250/888888/000000&text=becas' />
			<p>BECAS<p>
		</div>
	</div>
	<div class='slider2'>
		<div class='slide uno'>
			<p>La cocina es arte,<br/><strong>descrubre y perfecciona todas sus técnicas.</strong></p>
		</div>
		<div class='slide dos'>
			<p>Además de deliciosos,<br/><strong>haz tus platillos nutritivos.</strong></p>
		</div>
		<div class='slide tres'>
			<p>Aprende a convertir un evento ordinario<br/><strong>en algo extraordinario.</strong></p>
		</div>
		<div class='slide cuatro'>
			<p>Más dulce que un postre<br/><strong>es ser experto en repostería.</strong></p>
		</div>
		<div class='slide cinco'>
			<p>Despierta y sensibiliza tus sentidos<br/><strong>del gusto, olfato y vista.</strong></p>
		</div>
	</div>
	<div class='slider3'>
		<h3>EXCELENCIA CULINARIA<br/><strong>A TU MEDIDA</strong></h3>
		
	</div>
	<div class='diplomados'>
		<h3>Diplomados profesionales</h3>
		<p>Invertir en un diplomado en Gastronómica Internacional te garantiza un dominio de las técnicas clásicas y contemporáneas indispensables para llegar a ser un profesional exitoso.</p>
	</div>
</div-->





<script>


jQuery(function(){ 
	$('.smpl_hslide').each(function(){
		var dataClass=$(this).attr('class');
		switch(dataClass){
			default: $(this).smpl_hslide(); break;
			case 'smpl_hslide slideBackground':
			case 'smpl_hslide slideBackground loscursos':
				var fn_in=function(slide_out, slide_in, fn_out, origin){
						var _text_out=$('.slide-mark',slide_out), _img_out=$('.page-image',slide_out),
							_text_in=$('.slide-mark',slide_in), _img_in=$('.page-image',slide_in);
						_text_out.stop().animate({opacity:0,left:-40},250,function(){
							_img_out.stop().animate({left:-_img_out.width()},500, function(){ slide_out.hide(); });
							slide_in.show();
							_text_in.css({ opacity:0,left:40 });
							_img_in.css({left:_img_in.width()*2}).stop().animate({left:0},500,function(){
								_text_in.stop().animate({ opacity:1,left:0 },250);
								fn_out(slide_in, origin);
							});
						});
						
					},
					fn_out=function(slide_in, origin){
						origin.update();
					},
					fn_change=function(){},
					fn_start=function(origin){
						
					};
				$(this).smpl_hslide(fn_in,fn_out,fn_change,fn_start);
			//	$('.next',this).click();
				break;
			case 'smpl_hslide slideImage':
				var fnMod=function(x,len){ return x>=len? x-len: (x<0? x+len: x); };
				var fn_in=function(slide_out, slide_in, fn_out, origin){
						var allSlides=origin.find('div.slide');
						var _in=allSlides.index(slide_in);
						var _out=allSlides.index(slide_out);
						var x_0, x_1, x_2, dir=0;
							if	( _in==_out+1 || (_in<_out && _out!=_in+1) ){ dir= 1, x_0=fnMod(_in-2,allSlides.length), x_1=fnMod(_in-1,allSlides.length), x_2=fnMod(_in,allSlides.length); }
						else if	( _in==_out-1 || (_in>_out && _out!=_in-1) ){ dir=-1, x_0=fnMod(_in,allSlides.length), x_1=fnMod(_in+1,allSlides.length), x_2=fnMod(_in+2,allSlides.length); }
						else { fn_out(slide_in, origin); return false; }
						allSlides.each(function(){ $(this).children().hide(); });
						$(slide_in).children().show();
						if(dir==1){
							$('.img',allSlides.eq(x_0)).show().stop().css({ left:-400, top:110, width:256,height:352 }).animate({ left:105, top:110, width:256,height:352 },1000).children('img').css({opacity:0.7}).parents('.slide').css({zIndex:1});
							$('.img',allSlides.eq(x_1)).show().stop().css({ left:-160, top:0, width:320,height:440 }).animate({ left:-400, top:110, width:256,height:352 },1000).children('img').css({opacity:1}).animate({opacity:0.7},1000).parents('.slide').css({zIndex:2});
							$('.img',allSlides.eq(x_2)).show().stop().css({ left:105, top:110, width:256,height:352 }).animate({ left:-160, top:0, width:320,height:440 },1000).children('img').css({opacity:0.7}).animate({opacity:1},1000,function(){ fn_out(slide_in, origin); }).parents('.slide').css({zIndex:3});
						}else{
							$('.img',allSlides.eq(x_0)).show().stop().css({ left:-400, top:110, width:256,height:352 }).animate({ left:-160, top:0, width:320,height:440 },1000).children('img').css({opacity:0.7}).animate({opacity:1},1000,function(){ fn_out(slide_in, origin); }).parents('.slide').css({zIndex:3});
							$('.img',allSlides.eq(x_1)).show().stop().css({ left:-160, top:0, width:320,height:440 }).animate({ left:105, top:110, width:256,height:352 },1000).children('img').css({opacity:1}).animate({opacity:0.7},1000).parents('.slide').css({zIndex:2});
							$('.img',allSlides.eq(x_2)).show().stop().css({ left:105, top:110, width:256,height:352 }).animate({ left:-400, top:110, width:256,height:352 },1000).children('img').css({opacity:0.7}).parents('.slide').css({zIndex:1});
						}
						//origin.find('.button_nav').removeClass('active').eq(_in).addClass('active');
					},
					fn_out=function(slide_in, origin){
						//alert();
						origin.update();
					},
					fn_change=function(){ },
					fn_start=function(origin){
						var slides=origin.find('div.slide');
						slides.show();
						//slides.each(function(){ var me=$('.img img', this); me.parent().css('background','url('+me.attr('src')+')'); })
											// agregado como navegador extra
											origin.find('.button_nav').click(function(){
												var me=$(this);
												me.parent().siblings('.slide_nav').find('.link').eq(me.attr('rel')).click();
											});
					};
				$(this).smpl_hslide(fn_in,fn_out,fn_change,fn_start);
			//	$('.next',this).click();
				break;
		}
	});
});

$(window).resize(function(){
	// slides background
	var h=$(window).height()-$('#header').height();
	_em=$('.slideBackground').first();
	_em.height(h);
	$('.page-image',_em).height(h/2.3);
	$('.slide',_em).height(h);
	var zoom=$(window).width()/900, width=$(window).width(); //16 de padding
//	$('.slideImage').css({zoom: zoom});
//	$('.slideImage .slide_nav').css({top:~~((415+20)*zoom)});
	$('.slideImage .slide_nav').css({zoom:zoom});
	$('.slideImage .slide .img').css({zoom: zoom});
	$('.slideImage .slide .slide-mark').css({paddingTop:~~((415+50)*zoom),backgroundPosition:'center '+~~(415*zoom)+'px', backgroundSize:zoom*2800, left:-width/2, width: width });
	$('.slideImage .new_navigate').css({top:~~((415+50)*zoom), left:-width/2, width: width });
	$('.home-slide_3').css({ backgroundSize:'auto '+~~((480*zoom)+$('.home-slide_3 .home-slide-content').height())+'px' });
	if($(window).innerHeight() < $(window).innerWidth())
		$("#downarrows").css({left:$(window).innerWidth()-35,bottom:"50%"});
	else
		$("#downarrows").css({left:($(window).innerWidth()-28)/2,bottom:"4%"});

	var controller = $.superscrollorama();
	$("#espacio").css({position:"absolute",top:$(window).innerHeight()*0.7});
	
	//$(".smpl_hslide.slideBackground").first().css({position:'fixed',top:60,background:'#fff',width:"100%","z-index":1});
	//controller.addTween("#espacio", TweenMax.to( $('.smpl_hslide.slideBackground').first(), .5, {css:{top:-$(window).innerHeight()}, ease:Quad.easeOut}));
	//$(".slide-mainTitle").css({"padding-top":$(window).innerHeight()*0.4});

//	$(window).scrollTop(0);
	
});
/*
windowScroll=0, showFlag=-1;
$(window).scroll(function(){
	if($(window).scrollTop()<30 && windowScroll>$(window).scrollTop() && showFlag==1){
		showFlag=0;
		$('.smpl_hslide.slideBackground').first().css({width:"100%","z-index":1, display:'block',top:-$(window).innerHeight()}).animate({top:0}, function(){ showFlag==-1 });
	} else
	if($(window).scrollTop()>windowScroll && windowScroll<30 && showFlag==-1){
		showFlag==0;
		$('.smpl_hslide.slideBackground').first().css({top:0}).animate({top:-$(window).innerHeight()}, function(){ $(this).css({ display:'none'}); showFlag==1 });
	}
	windowScroll=$(window).scrollTop();
});*/

$(document).ready(function(){

	//$(".smpl_hslide.slideBackground, .slide-mainTitle, .smpl_tabs, .smpl_hslide slideImage").css({position:'absolute',background:'#fff',"z-index":-1});
	/*$(".smpl_hslide.slideBackground").first().css({position:'absolute',background:'#fff',width:"100%","z-index":1});
	
	window.requestAnimFrame = (function(){
		return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          function( callback ){
            window.setTimeout(callback, 1000 / 60);
          };
	})();*/

	$(".smpl_hslide.slideBackground").eq(0).append(
		"<img id='downarrows' src='<?php bloginfo('template_url'); ?>/mobile/img/down_arrows.png' style='position:absolute;bottom:4%;height:25px;' />"
	);
	$("#downarrows").click(function(){ $("html, body").animate({scrollTop:$(window).innerHeight()-38}); });
});

$(function(){
	$(window).resize();
	TweenMax.from($("#downarrows"),1,{bottom:"-10%",opacity:0,delay:1});
});

</script>

<?php /*

		<div class="smpl_hslide slideBackground">
			<div class="slide slide_0" data-icon="" style="display: none;">
				<div class="img">
					<img class="page-image" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAAEElEQVR42mL4//8/A0CAAQAI/AL+26JNFgAAAABJRU5ErkJggg==" style="background-image: url(http://gastronomica.lead2action.com/wp-content/uploads/2014/12/img_pantalla_2.png);">
				</div>
				<div class="slide-mark">
					<div class="slide-title">Asesoría Profesional a un clic de distancia</div>
					<a href="http://google.com.mx" class="link">Conoce más</a>
				</div>
			</div>
			<div class="slide slide_1 active" data-icon="">
				<div class="img">
					<img class="page-image" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAAEElEQVR42mL4//8/A0CAAQAI/AL+26JNFgAAAABJRU5ErkJggg==" style="background-image: url(http://gastronomica.lead2action.com/wp-content/uploads/2014/12/img_pantalla_1.png);">
				</div>
				<div class="slide-mark">
					<div class="slide-title">Haz de tu cocina tu taller gastronomico</div>
					<a href="http://google.com.mx" class="link">Conoce nuestro diplomado en cocina</a>
				</div>
			</div>
			<div class="slide slide_2" data-icon="" style="display: none;">
				<div class="img">
					<img class="page-image" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAAEElEQVR42mL4//8/A0CAAQAI/AL+26JNFgAAAABJRU5ErkJggg==" style="background-image: url(http://gastronomica.lead2action.com/wp-content/uploads/2014/12/img_pantalla_3.png);">
				</div>
				<div class="slide-mark">
					<div class="slide-title">Prestigio y Validez Internacional</div>
					<a href="http://google.com.mx" class="link">Conoce más</a>
			</div>
			</div>
		</div>
		
		
		
		
		<div class="smpl_hslide slideBackground loscursos">
			<div class="slide slide_0" data-icon="/wp-content/uploads/2014/12/btn_cocina.png" style="display: none;">
				<div class="img">
					<img class="page-image" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAAEElEQVR42mL4//8/A0CAAQAI/AL+26JNFgAAAABJRU5ErkJggg==" style="background-image: url(http://gastronomica.lead2action.com/wp-content/uploads/2014/12/back_cocina.png);">
				</div>
				<div class="slide-mark">
					<!--div class="slide-title">Cocina</div-->
					<div class="slide-content">
						La cocina es un arte,<br>
						<strong>descubre y perfecciona todas sus técnicas.</strong>
					</div>
					<a href="http://google.com.mx" class="link">Cocina profesional</a>
				</div>
			</div>
			<div class="slide slide_1 active" data-icon="/wp-content/uploads/2014/12/btn_nutricion.png" style="display: block;">
				<div class="img">
					<img class="page-image" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAAEElEQVR42mL4//8/A0CAAQAI/AL+26JNFgAAAABJRU5ErkJggg==" style="background-image: url(http://gastronomica.lead2action.com/wp-content/uploads/2014/12/back_nutricion.png);">
				</div>
				<div class="slide-mark">
					<!--div class="slide-title">Ensaladas</div-->
					<div class="slide-content">
						Además de deliciosos,<br>
						<strong>haz tus platillos nutritivos.</strong>
					</div>
					<a href="http://google.com.mx" class="link">Nutrición</a>
				</div>
			</div>
			<div class="slide slide_2" data-icon="/wp-content/uploads/2014/12/btn_organizacion.png" style="display: none;">
				<div class="img">
					<img class="page-image" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAAEElEQVR42mL4//8/A0CAAQAI/AL+26JNFgAAAABJRU5ErkJggg==" style="background-image: url(http://gastronomica.lead2action.com/wp-content/uploads/2014/12/back_organizacion.png);">
				</div>
				<div class="slide-mark">
					<!--div class="slide-title">Organización</div-->
					<div class="slide-content">
						Aprende a convertir un evento ordinario<br>
						<strong>en uno extraordinario.</strong>
					</div>
					<a href="http://google.com.mx" class="link">Organización y administración de eventos</a>
				</div>
			</div>
			<div class="slide slide_3" data-icon="/wp-content/uploads/2014/12/btn_reposteria.png" style="display: none;">
				<div class="img">
					<img class="page-image" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAAEElEQVR42mL4//8/A0CAAQAI/AL+26JNFgAAAABJRU5ErkJggg==" style="background-image: url(http://gastronomica.lead2action.com/wp-content/uploads/2014/12/back_reposteria.png);">
				</div>
				<div class="slide-mark">
					<!--div class="slide-title">Postres</div-->
					<div class="slide-content">
						Más dulce que un postre<br>
						<strong>es ser experto en repostería.</strong>
					</div>
					<a href="http://google.com.mx" class="link">Repostería profesional</a>
				</div>
			</div>
			<div class="slide slide_4" data-icon="/wp-content/uploads/2014/12/btn_vino.png" style="display: none;">
				<div class="img">
					<img class="page-image" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAAEElEQVR42mL4//8/A0CAAQAI/AL+26JNFgAAAABJRU5ErkJggg==" style="background-image: url(http://gastronomica.lead2action.com/wp-content/uploads/2014/12/back_vino.png);">
				</div>
				<div class="slide-mark">
					<!--div class="slide-title">Vinos</div-->
					<div class="slide-content">
						Despierta y sensibiliza tus sentidos<br>
						<strong>del gusto, olfato y vista.</strong>
					</div>
					<a href="http://google.com.mx" class="link">El mundo del vino</a>
				</div>
			</div>
		</div>
		
		
		
		
		
		<div class="smpl_hslide slideImage">
			<div class="slide slide_0" data-icon="/wp-content/uploads/2014/12/icon_chef_3.png">
				<div class="img">
					<img class="page-image" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAAEElEQVR42mL4//8/A0CAAQAI/AL+26JNFgAAAABJRU5ErkJggg==" style="background-image: url(http://gastronomica.lead2action.com/wp-content/uploads/2014/12/chef_3.png);">
				</div>
				<div class="slide-mark" style="display: none;">
					<div class="slide-title">Carreras técnicas</div>
					<div class="slide-content">
						Una opción ideal para quienes poseen formación previa y desean perfeccionar sus conocimientos. Los módulos de los cursos técnicos Gastronómica Internacional pueden tomarse de manera independiente como cursos profesionales.
					</div>
				</div>
			</div>
			<div class="slide slide_1 active" data-icon="/wp-content/uploads/2014/12/icon_chef_2.png">
				<div class="img">
					<img class="page-image" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAAEElEQVR42mL4//8/A0CAAQAI/AL+26JNFgAAAABJRU5ErkJggg==" style=""background-image: url(http://gastronomica.lead2action.com/wp-content/uploads/2014/12/chef_2.png);">
				</div>
				<div class="slide-mark" style="display: block;">
					<div class="slide-title">Cursos introductorios</div>
					<div class="slide-content">
						Escoge tu cocina favorita y especialízate. Gastronómica Internacional te ofrece cursos temáticos en Pastelería clásica, Chocolatería profesional, Mousses y bavaresas, Tartas básicas y más.
					</div>
				</div>
			</div>
			<div class="slide slide_2" data-icon="/wp-content/uploads/2014/12/icon_chef_1.png">
				<div class="img">
					<img class="page-image" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAAEElEQVR42mL4//8/A0CAAQAI/AL+26JNFgAAAABJRU5ErkJggg==" style="background-image: url(http://gastronomica.lead2action.com/wp-content/uploads/2014/12/chef_1.png);">
				</div>
				<div class="slide-mark" style="display: none;">
					<div class="slide-title">Diplomados profesionales</div>
					<div class="slide-content">Invertir en un diplomado Gastronómica Internacional te garantiza un dominio de las técnicas clásicas y contemporáneas indispensables para llegar a ser un profesional exitoso.</div>
				</div>
			</div>
		</div>
*/ ?>

<div id='espacio'></div>

<?php

// carga las opciones de la plantilla
$options=get_option('pa_opciones');

// carga los hijos de la página HOME (sabe quién es home porque lo toma de las opciones de la plantilla)
$home_pages=array();
if(is_array($options) and isset($options['web_home_id']))
	$home_pages=get_pages(array('parent'=>$options['web_home_id'],
	'sort_order' => 'ASC',
	'sort_column' => 'menu_order'
	));
	
	
	// ***** WIDGETS ****
	foreach($home_pages as $k=>$widget){
?>		<div class="home-slide home-slide_<?php echo $k; ?>">
			<div class="home-slide_mainContainer">
<?php	// carga las variables de la página
		$widget_vars=get_post_custom($widget->ID);
		//  imprime titulo y contenido del widget ?>
		<h2 class="home-slide-title"><?php $widget->post_title ?></h2>
		<div class="home-slide-content"><?php echo $widget->post_content; ?></div><?php
		// Inerta el widget
		if(isset($widget_vars['widget-type']))
		switch($widget_vars['widget-type'][0]){
			// ****** DEFAULT
			default: echo "<pre>"; print_r($widget); echo "</pre>"; break;
			// ****** HSLIDE
			case 'hslide': ?>
				<div class="smpl_hslide <?php if(isset($widget_vars['widget-class'])) echo implode(' ',$widget_vars['widget-class']); ?>" ><?php
					// carga los slides del horizontal slide
					$slide_pages=get_pages(array('parent'=>$widget->ID,'sort_order' => 'ASC', 'sort_column' => 'menu_order'));
					if(count($slide_pages)>0)
					foreach($slide_pages as $kk=>$slide){ 
						$slide_vars=get_post_custom($slide->ID); ?>
					<div class="slide slide_<?php echo $kk;?>" data-icon="<?php echo $slide_vars['icon'][0]; ?>" rel="<?php echo $kk;?>">
						<?php if(isset($slide_vars['image']) or isset($slide_vars['image-mobile'])){ ?>
						<div class="img">
							<img class="page-image" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAAEElEQVR42mL4//8/A0CAAQAI/AL+26JNFgAAAABJRU5ErkJggg=="
							style="background-image:url(<?php echo isset($slide_vars['image-mobile'])? $slide_vars['image-mobile'][0]: $slide_vars['image'][0]; ?>);" />
						</div><?php } ?>
						<div class="slide-mark">
							<div class="slide-title"><?php echo isset($slide_vars['title'])? $slide_vars['title'][0]:  $slide->post_title; ?></div>
							<div class="slide-content"><?php echo $slide->post_content; ?></div>
							<?php if(isset($slide_vars['link-href'][0])){ ?>
							<a href="<?php echo $slide_vars['link-href'][0]; ?>" class="link">
								<span class="icon" style="background-image:url(<?php echo $slide_vars['icon'][0]; ?>)"></span>
								<?php echo $slide_vars['link-text'][0]; ?>
							</a><?php } ?>
						</div>
					</div><?php 
					}
					// si no hay slides en el horizontal silide lo anuncia
					else { ?><div class="admin-alert">la página no tiene páginas contenidas. Agrega páginas inferiores.</div><?php }
				?>
					<div class="new_navigate" style="display:none;">
					<?php	foreach($slide_pages as $kk=>$slide){ ?><a class="button_nav" rel="<?php echo $kk; ?>"></a><?php } ?>
					</div>
					<div class="clearBoth"></div>
				</div>
			<?php break;
			case 'tabs': ?>
				<div class="slide-mainTitle">Una formación<br><b>gastronómica integral.</b></div>
				<div class="smpl_tabs <?php if(isset($widget_vars['widget-class'])) echo implode(' ',$widget_vars['widget-class']); ?>"><?php
				// carga los tabs del horizontal slide
				$tabs_pages=get_pages(array('parent'=>$widget->ID,'sort_order' => 'ASC', 'sort_column' => 'menu_order'));
				if(count($tabs_pages)>0)
				foreach($tabs_pages as $tab){
					$tab_vars=get_post_custom($tab->ID); ?>
					<div class="tab">
						<?php if(isset($tab_vars['link-href'][0])){ ?>
						<a href="<?php echo $tab_vars['link-href'][0]; ?>" class="link">
							<?php if(isset($tab_vars['image']) or isset($tab_vars['image-mobile'])){ ?>
							<img class="img-tab" src="<?php echo isset($tab_vars['image-mobile'])? $tab_vars['image-mobile'][0]: $tab_vars['image'][0]; ?>" /><?php } ?>
							<div class="tab-title"><?php echo $tab->post_title; ?></div>
							<!--<?php echo $tab_vars['link-text'][0]; ?>-->
						</a><?php } ?>
					</div><?php
				}
				?>
				</div>
			<?php break;
		};
?>			</div>
		</div>
<?php	
	}
?>