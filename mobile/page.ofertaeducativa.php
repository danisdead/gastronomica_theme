<?php


function getArrayBreadCrumbs($id, $id_home){
	$breadcrumbs=array();
	$page=get_page( $id );
	if($page->post_parent and $page->ID!=$id_home)
		$breadcrumbs=getArrayBreadCrumbs($page->post_parent, $id_home);
	if($page->ID==$id_home) $breadcrumbs[$page->ID]='<a href="'.$page->guid.'">Home</a>';
					else	$breadcrumbs[$page->ID]='<a href="'.$page->guid.'">'.$page->post_title.'</a>';
	return $breadcrumbs;
}


// carga las opciones de la plantilla
$options=get_option('pa_opciones');	
	
$page=get_page( get_the_ID() );
$page_vars=get_post_custom( get_the_ID() );
$breadcrumsArray=getArrayBreadCrumbs( $page->ID, $options['web_menu_id'] );
$depth=count($breadcrumsArray);
?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile/css/pages.css">
	<!--link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile/css/ofertaeducativa.css"-->

	<div id="primary" class="content-area">
		<div id="pagecontent" class="site-content" role="main">

			<div class="breadcrumbs"><?php echo implode('<span>&gt;</span>', $breadcrumsArray ); ?></div>
			
			<div class="ofertaeducativa">

<?php
	switch($depth){
		// Nivel 0
		case 1: ?>
				<div class="nivel1">
					<div class="pagecontent_title"><span>Conoce nuestra </span><?php echo $page->post_title; ?></div>
					<ul class="ofertaeducativa_mainmenu">		
					<?php
						// toma el listado de páginas
						$pages = get_pages( array('parent' => $page->ID, 'sort_order' => 'ASC', 'sort_column' => 'menu_order' ) );
						// recorre la lista de páginas
						foreach($pages as $k=>$page){
							// tova las variables configurables de cáda págia
							$vars=get_post_custom($page->ID);
							$href=isset($vars['link-href'])? current($vars['link-href']): $page->guid;
							// dibuja la liga ?>
							<li>
								<a href="<?php echo $href; ?>">
									<img src="<?php echo isset($vars['image-mobile'])? $vars['image-mobile'][0]: $vars['image'][0]; ?>">
									<span><?php echo $page->post_title; ?></span>
								</a>
							</li><?php
						}
					?>
					</ul>
				</div>
<?php		break;
		// Nivel 1
		case 2:
			$parent=get_page($page->post_parent); ?>
				<div class="nivel2">
					<div class="ofertaeducativa_title"><span><?php echo $parent->post_title; ?></span><br>en <?php echo $page->post_title; ?></div>
					<div class="content">
						<?php $parent=get_page( $page->post_parent ); ?>
						<div class="nav"><a href="<?php echo $parent->guid; ?>">
							<span class="arrow">&lt;</span>
							<span class="label">Regresar a</span>
							<span class="title"><?php echo $parent->post_title; ?></span>
						</a></div>
						<ul class="content_menu">	
							<?php
								// toma el listado de páginas
								$children = get_pages( array('parent' => $page->ID, 'sort_order' => 'ASC', 'sort_column' => 'menu_order' ) );
								// recorre la lista de páginas
								foreach($children as $k=>$child){
									// tova las variables configurables de cáda págia
									$vars=get_post_custom($child->ID);
									$href=isset($vars['link-href'])? current($vars['link-href']): $child->guid;
									// dibuja la liga ?>
									<li>
										<a href="<?php echo $href; ?>">
											<img src="<?php echo isset($vars['image-mobile'])? $vars['image-mobile'][0]: (isset($vars['image'])?$vars['image'][0]:''); ?>" class="image">
											<span class="title"><?php echo $child->post_title; ?></span>
											<img src="<?php echo isset($vars['icon'])?$vars['icon'][0]:''; ?>" class="icon">
										</a>
									</li><?php
								}
							?>
						</ul>
					</div>
					<ul class="submenu">
						<div class="title"><?php echo $parent->post_title; ?></div>
						<?php
							// toma el listado de páginas
							$siblings = get_pages( array('parent' => $page->post_parent, 'sort_order' => 'ASC', 'sort_column' => 'menu_order' ) );
							// recorre la lista de páginas
							foreach($siblings as $k=>$sibling){
								// tova las variables configurables de cáda págia
								$vars=get_post_custom($sibling->ID); 
								$href=isset($vars['link-href'])? current($vars['link-href']): $sibling->guid;
								$class=$sibling->ID==$page->ID?'active':'';
								// dibuja la liga ?>
								<li>
									<a href="<?php echo $href; ?>" class="<?php echo $class; ?>">
										<span><?php echo $sibling->post_title; ?></span>
									</a>
								</li><?php
							}
						?>
					</ul>
				</div>
<?php		break;
		// Nivel 2
		case 3:
		case 4:

			if($depth==4){
				$current=$page;
				$page=get_page( $page->post_parent );
				$page_vars=get_post_custom( $page->ID );	
			}else{
				$current=reset(get_pages( array('parent' => $page->ID, 'sort_order' => 'ASC', 'sort_column' => 'menu_order' ) ));
			}
			$parent=get_page($page->post_parent);
			// toma el listado de páginas
			$siblings = get_pages( array('parent' => $page->post_parent, 'sort_order' => 'ASC', 'sort_column' => 'menu_order' ) );
			// busca la posición de la página actual
			foreach($siblings as $k=>$sibling) if($sibling->ID==$page->ID) {
				$prev=isset($siblings[$k-1])? $siblings[$k-1]: $siblings[count($siblings)-1];
				$next=isset($siblings[$k+1])? $siblings[$k+1]: $siblings[0];
				break;
			}
		?>
				<div class="nivel3">
					<div class="ofertaeducativa_title" style="background-image:url(<?php echo isset($page_vars['icon'])?$page_vars['icon'][0]:''; ?>);">
						<?php echo isset($page_vars['HtmHeaderWeb'])?$page_vars['HtmHeaderWeb'][0]:$parent->post_title; ?></div>
					<div class="content">
						<div class="nav">
							<a class="prev" href="<?php echo $prev->guid; ?>"><span class="arrow">&lt;</span><span class="title"><?php echo $prev->post_title; ?></span></a>
							<a class="next" href="<?php echo $next->guid; ?>"><span class="arrow">&gt;</span><span class="title"><?php echo $next->post_title; ?></span></a>
						</div>
						<div id="ajaxable_content"><?php echo $current->post_content; ?></div>
					</div>
					
					
					
					
					
					
					
					
	
				<div class="clearBoth"></div>


<?php

							$current_vars=get_post_custom( $current->ID );
							if(isset($current_vars['contribuidor'])){
							?><ul class="docentes" id="contribuidores" ><?php
								foreach($current_vars['contribuidor'] as $contribuidor){
									$contribuidor=get_page( (int)$contribuidor );
									// tova las variables configurables de cáda págia
									$vars=get_post_custom($contribuidor->ID); 
									$href=isset($vars['link-href'])? current($vars['link-href']): $contribuidor->guid;
									// dibuja la liga ?>
									<li data-id="<?php echo $contribuidor->ID; ?>" >
										<div class="data_preview">
											<div class="img"><img src="<?php echo isset($vars['image'])?$vars['image'][0]:''; ?>"></div>
											<div class="name"><?php echo $contribuidor->post_title; ?></div>
											<div class="title"><?php echo isset($vars['title'])?$vars['title'][0]:'&nbsp;'; ?></div>
											<div class="countries">
												<?php
												if(isset($vars['countries'])){
													$vars['countries'][0]=explode(',',$vars['countries'][0]);
													foreach ($vars['countries'][0] as $contry) {
														?><span class="f_<?php echo  $contry; ?>"></span><?php
													}
												}
												?>
											</div>
										</div>
										<div class="data_content" style="display:none;">
											<!--div class="background"></div-->
											<div class="content_content">
												<!--div class="img"><img src="<?php echo isset($vars['image'])?$vars['image'][0]:''; ?>"></div>
												<div class="name"><?php echo $contribuidor->post_title; ?></div>
												<div class="title"><?php echo isset($vars['title'])?$vars['title'][0]:'&nbsp;'; ?>
													<div class="countries">
														<?php
														if(isset($vars['countries'])){
															foreach ($vars['countries'][0] as $contry) {
																?><span class="f_<?php echo  $contry; ?>"></span><?php
															}
														}
														?>
													</div>
												</div-->
												<div class="description">
													<?php echo $contribuidor->post_content; ?>
													<div class="clearBoth"></div>
												</div>
												<a class="link" href="<?php echo isset($vars['link-href'])?$vars['link-href'][0]:'#'; ?>">
													<?php echo isset($vars['link-title'])?$vars['link-title'][0]:'Ver sus artículos'; ?>
												</a>
												<a class="close"></a>
											</div>
										</div>
									</li><?php
								}
						?></ul>
<?php					}
						?><div class="clearBoth"></div><?php
						// imprime el pie si lo tiene
						if(isset($current_vars['pie'])){ ?><div class="content_footer"><?php echo implode(' ',$current_vars['pie']); ?></div><?php }

						?>
<script language="javascript">
$(function(){
	var _docente= $('#contribuidores');
	var _lis=_docente.children();
	var _cnt=$('.data_content',_lis);
	var currentScrollTop;
	_lis.click(function(){
		currentScrollTop=$(window).scrollTop();
		
		var me=$(this);
		var topDelta=me.offset().top-$(window).scrollTop();
		_lis.each(function(){ $(this).removeClass('hover'); });
		me.addClass('hover');
		$('html, body').animate({scrollTop:me.offset().top-topDelta},1, function(){
			var content_content=$('.content_content',me);
			$('html, body').animate({scrollTop:content_content.offset().top-100});
		});
		
	})
	_cnt.click(function(event){
		event = event || window.event;
		event.stopPropagation();
	});
	_lis.filter('[data-id="<?php echo $current_ID; ?>"]').click();
	$('.close').click(function(){
		_lis.filter('.hover').find('.data_preview').stop().css({ marginBottom: '' });
		$("html, body").stop().animate({ scrollTop:currentScrollTop});
		var me=$(this);
		_lis.each(function(){
			$(this).removeClass('hover');
		});
	});
});
</script>

				
					
					<div class="clearBoth"></div>
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					<ul class="submenu" id="level2_linkajax">
						<div class="title">Programa</div>
						<?php
							// toma el listado de páginas
							$childs = get_pages( array('parent' => $page->ID, 'sort_order' => 'ASC', 'sort_column' => 'menu_order' ) );
							// recorre la lista de páginas
							foreach($childs as $k=>$child){
								// tova las variables configurables de cáda págia
								$vars=get_post_custom($child->ID); 
								$href=isset($vars['link-href'])? current($vars['link-href']): $child->guid;
								if($k==0) $href=$page->guid;
								// dibuja la liga ?>
								<li>
									<a href="<?php echo $href ?>" class="<?php echo $current->ID==$child->ID?'active':''; ?>">
										<span><?php echo $child->post_title; ?></span>
									</a>
								</li><?php
							}
						?>
					</ul>
					<!--ul class="submenu duration">
						<div class="title">Duración</div>
						<li><?php echo isset($page_vars['duracion'])?$page_vars['duracion'][0]:''; ?></li>
					</ul-->
				</div>
<?php		break;
	}
?>					




				<div class="clearBoth"></div>
				
			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

	
<script>



function loadDocenteLinks(){
	$('[data-urldocente]').each(function(){
		var me=$(this); 
		$.getJSON(me.attr('data-urldocente'),function(data){
			if(data instanceof Object)
			me.html(
											
				'<div class="image"><img src="'+( (data.vars && data.vars.image)? data.vars.image.join(' '): '') +'">'+
					'<div class="over"><a href="'+me.attr('data-urldocente')+'"></a></div>'+
				'</div>'+
				'<div class="name">'+( (data.post_title && data.post_title)? data.post_title: '') +'</div>'+
				'<div class="title">'+( (data.vars && data.vars.title)? data.vars.title.join(' '): '') +'</div>'+
				'<div class="countries"><span class="f_'+( (data.vars && data.vars.countries)? data.vars.countries.join('').split(',').join('"></span><span class="f_'): '')+'"></span>'
			);
		});
	});
}

$(function(){
	$('.ofertaeducativa_mainmenu a, .content_menu a').on('touchstart click',function(){
		$('.ofertaeducativa_mainmenu a, .content_menu a').removeClass('hover');
		$(this).addClass('hover');
		
	}).on('touchend mouseup',function(){
		$(this).removeClass('hover');
	});
	loadDocenteLinks();
})
</script>
