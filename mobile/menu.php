<?php
// Requerido por header.php


// funcion que dibuja el men�
function createMenu($parent=0, $level=0, $nm="", $href="#", $trgt="_self", $a_active=""){
	if($level>=3) return false;
	// toma el listado de p�ginas
	$pages = get_pages( array('parent' => $parent, 'sort_order' => 'ASC', 'sort_column' => 'menu_order' ) );
	$len = count($pages);
	if($len>0){
		ob_start();
		// dibuja la lista del menu ?>
		<ul class="<?php echo 'level_'.$level; ?>">
		<?php if($level > 0){ ?>
			<li class='back'><a href='javascript:void(0)'><span>Regresar</span></a></li>
			<li class='anchor <?php print $a_active; ?>'><a href="<?php print $href; ?>" target="<?php print $trgt; ?>"><span><?php echo $nm; ?></span></a></li>
		<?php }
		$last_index=$len-1; // indice del �ltimo elemento del nivel
		$active_id=get_the_ID(); // id de la p�gina activa
		$i=0;
		// recorre la lista de p�ginas
		foreach($pages as $page){
			// tova las variables configurables de c�da p�gia
			$vars=get_post_custom($page->ID);
			// agrega las clases de estilo
			$clases=array();
				$clases[]='level_'.$level;
				$clases[]= $i==0? 'first': ( $i==$last_index? 'last': 'middle');
				//$clases[]= ($i%2)==0? 'par': 'non';
				if($active_id == $page->ID) $clases[]='active';
				if(isset($vars['link-class'])) $clases[]=implode(' ',$vars['link-class']);
			// direcci�n final
			$href=isset($vars['link-href'])? current($vars['link-href']): get_permalink( $page->ID );
			$target=isset($vars['link-target'])? current($vars['link-target']): '_self';
			// dibuja la liga ?>
				<?php
					$str_tmp = trim(createMenu($page->ID, $level+1,$page->post_title,$href,$target,($active_id == $page->ID ? "active" : "") ));
					if(strpos($str_tmp,'active')!=false) $clases[]='active';
					
					if(($str_tmp == "") || ($page->post_title == "Cuerpo docente")){
				?>
					<li class="<?php echo implode(' ',$clases); ?> anchor">
						<a href="<?php echo $href; ?>" target="<?php echo $target; ?>"><span><?php echo isset($vars['link-mobileTitle'])?$vars['link-mobileTitle'][0]:$page->post_title; ?></span></a>
					</li>
				<?php
						// dubuja los hijos
					}else{
				?>
					<li class="<?php echo implode(' ',$clases); ?> dropdown">
						<a href="javascript:void(0)"><span><?php echo isset($vars['link-mobileTitle'])?$vars['link-mobileTitle'][0]:$page->post_title; ?></span></a>
				<?php
						print $str_tmp;
				?></li><?php
					}
			$i++; // incremente i, para llevar la relacion de primero y ultimo, par y non
		}
		// dibuja el fin del menu ?>
			<li class="clearboth"></li>
		</ul><?php
		$out_str = ob_get_contents();
		ob_end_clean();
		return $out_str;
	}else{
		return "";
	}
}

// carga las opciones de la plantilla
$options=get_option('pa_opciones');

/*
if(is_array($options) and isset($options['web_campusv_id']) and $options['web_campusv_id']!=""){
	$campusv_page=get_page(array('ID'=>$options['web_campusv_id']));
	if($campusv_page){?>
		<!---- MAIN MENU ----->
		<div class="menu_topbar">
			<div class="menu_topbar_container">
				<div class="telephon">
					<span>+52 55 5207 5443</span>
					<span>+52 55 5207 7485</span>
				</div>
				<div class="menu_campus_virtual">
					<!--a href="<?php echo get_permalink($campusv_page->ID); ?>"><span><?php echo $campusv_page->post_title; ?></span></a-->
					<a href="#"><span>Campus Virtual</span></a>
				</div>
			</div>
		</div><?php
	}
}
*/

	print createMenu(0);


/*
echo "<pre style='text-align:left;'>"; print_r( get_pages( array('parent' => 0) ) ); echo "</pre>";
echo "-<pre style='text-align:left;'>"; print_r( get_post_custom(2)['horales']); echo "</pre>-";
*/
