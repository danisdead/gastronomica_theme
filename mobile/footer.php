<?php

// carga las opciones de la plantilla
$options=get_option('pa_opciones');

?>

	<script>
	$('a.link, .submit').on('touchstart click',function(){
		$('a.link').removeClass('hover');
		$(this).addClass('hover');
	}).on('touchend mouseup',function(){
		$(this).removeClass('hover');
	});
	</script>



	<div id="footer">

		<div class='siguenos'>
			<p>Síguenos</p>
			<a href='http://www.facebook.com/GastronomicaInternacional' target='_blank'><img src='<?php bloginfo('template_url'); ?>/mobile/img/general/facebook_icon_light.png' /></a>
			<a href='http://www.youtube.com/channel/UC9xKIOOQDEyL5d1xxYJgkOQ' target='_blank'><img src='<?php bloginfo('template_url'); ?>/mobile/img/general/youtube_icon_light.png' /></a>
			<a href='http://instagram.com/_gastronomica' target='_blank'><img src='<?php bloginfo('template_url'); ?>/mobile/img/general/instragram_icon_light.png' /></a>
			<a href='http://twitter.com/@_gastronomica' target='_blank'><img src='<?php bloginfo('template_url'); ?>/mobile/img/general/twitter_icon_light.png' /></a>
			<div class='barra'></div>
		</div>
		<div class='legales'>
			<a class='privacidad' href='<?php echo site_url(); ?>/menu/avisos-de-privacidad/'>Avisos de privacidad.</a>
			<a class='terminos' href='<?php echo site_url(); ?>/menu/terminos-y-condiciones/'>Términos y condiciones.</a>
		</div>
		<div class='desktop'>
			<a href='?mode=desktop'>Ver versión desktop</a>
			<div class='barra'></div>
		</div>
		<div class='copyright'>
			<p class='copy'>Copyright &copy; 2015. Gastronómica Inernacional.</p>
			<p class='rights'>Todos los derechos reservados</p>
		</div>
	
	</div>
</div>
</body>
</html>
