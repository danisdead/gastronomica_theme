<!DOCTYPE html>
<html lang="es-MX">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->
		<title>Gastronomica</title>
		<?php
				wp_head();			
		?>
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
		<link rel='stylesheet' id='open-sans-css'  href='//fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&#038;subset=latin%2Clatin-ext&#038;ver=4.1' type='text/css' media='all' />
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile/css/style.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile/css/menu.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile/css/header.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile/css/footer.css">
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

		<!-- flags -->
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/flags/flags.css">
		<!-- jPaler -->
		<link type="text/css" href="<?php bloginfo('template_url'); ?>/plugins/jPlayer/css/jplayer.blue.monday.css" rel="stylesheet" />
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/plugins/jPlayer/jquery.jplayer.min.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/plugins/jPlayer/jplayer.js"></script>

		<script src="<?php bloginfo('template_url'); ?>/web/js/superscrollorama/greensock/TweenMax.min.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/web/js/superscrollorama/jquery.superscrollorama.js"></script>
								
		<!-- css3-mediaqueries.js for IE less than 9 -->
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile/css/home.css">
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.smpl_slides.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/mobile/js/menu.js"></script>
		
		<script src="<?php bloginfo('template_url'); ?>/web/js/share.js"></script>
		
		<!--- ANALYTCIS --->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-46606503-1', 'auto');
		  ga('require', 'displayfeatures');
		  ga('send', 'pageview');

		</script>
		<!--- FIN ANALYTICS --->
		
                <!--Start of Zopim Live Chat Script-->
                <script type="text/javascript">
                 window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
                 d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
                 _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
                 $.src="//v2.zopim.com/?3BGT8oFDBBm0oecNp9E0iBhFcd4Valw2";z.t=+new Date;$.
                 type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
                </script>
                <!--End of Zopim Live Chat Script-->		
		
	</head>
	<body>
                <div id="wrapper">
			<div id="header">
				<div class='head_logo'></div>
				<div class="main_menu">
					<div id='mb_menu_btn'></div>
<?php /*
<h2 ><?php _e('Categories'); ?></h2>
<ul > 
<?php wp_list_cats('sort_column=name&optioncount=1&hierarchical=0'); ?>
</ul>
<h2 ><?php _e('Archives'); ?></h2>
<ul >
<?php wp_get_archives('type=monthly'); ?>
</ul>
</div> */?>


<?php 
	//wp_page_menu( $args );
	//echo "<pre style='text-align:left;'>"; print_r( get_pages(  )); echo "</pre>";
	//echo "-<pre style='text-align:left;'>"; print_r( get_post_custom(2)['horales']); echo "</pre>-";
	require('menu.php'); 
?> 


				</div>
				<div class='head_contact'>
					<div id='mb_contact_btn'></div>
				</div>
			</div>
			<div id="header_dummy"></div>
			