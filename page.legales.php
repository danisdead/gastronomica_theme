<?php
/*
Template Name: Legales
*/

// si es solicitado vía ajax responde el objeto
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest'){
	header('Content-Type:application/json');
	echo json_encode( get_page( get_the_ID() ) );
	exit();
}

get_header();
	require( ((wp_is_mobile() && ($_SESSION["m_desktop"]!=true)) ? 'mobile': 'web').'/page.legales.php' );
get_footer();
