<?php

// carga las opciones de la plantilla
$options=get_option('pa_opciones');

	// es categoría o es un artículo
	$isCategory=get_the_category();
	$isPost=is_single();
	$isAuthor=is_author();
	
	// carga la categoría
	$current_categories = get_the_category();
	$category_slug = get_queried_object()->slug;
	$category=current($current_categories);
		foreach($current_categories as $cat)
			if($category_slug==$cat->slug) $category=$cat;
		
	// carga el artículo
	$post = get_post();
	$post_slug = $post->post_name;
	
	//carga el autor
	$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
	$author = $author->data;
	
	// urlCarpeta
	$urlCaerpeta=$isAuthor?'/blog/autor/'.$author->user_nicename:'';
	
// variables principales
$description = $isPost?
	'Categoría: '.($isCategory? $category->name: '')
	: 'Conoce lo último en tendencias en nuestros artículos.'; 
?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/web/css/articulosculinarios.css">

	<div id="primary" class="content-area">
		<div id="pagecontent" class="site-content" role="main">
			 <!-- Articulos Culinarios -->
            <ol vocab="http://schema.org/" typeof="BreadcrumbList" class="breadcrumbs">
            	<li property="itemListElement" typeof="ListItem">
            		<a property="item" typeof="WebPage" href="<?php echo get_site_url(); ?>" >
                    <span property="name">HOME</span>
                    </a>
                    <meta property="position" content="1">
                    <span>&gt;</span>
				</li>
                <li property="itemListElement" typeof="ListItem">
            		<a property="item" typeof="WebPage" href="<?php echo get_site_url(); ?>/articulos-culinarios" >
                    <span property="name">Artículos Culinarios</span>
                    </a>
                    <meta property="position" content="2">
                    <span>&gt;</span>
				</li>
				<?php if($isAuthor){ ?>
                <li property="itemListElement" typeof="ListItem">
            		<a property="item" typeof="WebPage" href="<?php echo get_site_url(); ?><?php echo $urlCaerpeta; ?>" class="active">
                    <span property="name"><?php echo $author->display_name; ?></span>
                    </a>
                    <meta property="position" content="3">
                    <span>&gt;</span>
				</li>
				<?php } 
					else if($isCategory)
				{ 
					$the_cat = get_the_category();
					$category_name = $the_cat[0]->cat_name;
					$category_link = get_category_link( $the_cat[0]->cat_slug ); 
				?>
                 <li property="itemListElement" typeof="ListItem">
            		<a property="item" typeof="WebPage" href="<?php echo get_site_url().'/articulos-culinarios/'.$category->slug; ?>" class="active">
                    <span property="name"><?php echo $category->name; ?></span>
                    </a>
                    <meta property="position" content="3">
                    <span>&gt;</span>
				</li>
				<?php } if($isPost){ ?>
                <li property="itemListElement" typeof="ListItem">
            		<a property="item" typeof="WebPage" href="<?php echo get_site_url().'/articulos-culinarios/'.$category->slug.'/'.$post_slug;?>" class="active">
                    <span property="name"><?php echo $post->post_title; ?></span>
                    </a>
                    <meta property="position" content="3">
				</li>
				<?php } ?>
            </ol>
			
       
            
            
			
			<div class="articulosculinarios">
				<div class="nivel2">
					<div class="articulosculinarios_title"><span>Artículos</span><br>Culinarios</div>
					<div class="articulosculinarios_content"><?php echo $description; ?></div>
					<div class="content">
<?php
	if($isPost){
	// *********** ARTICULO 

		// 
		$post= $posts[0];
		
			$cats=wp_get_post_categories( $post->ID );
		
		$image=get_the_post_thumbnail( $post->ID );
		$date = new DateTime($post->post_date); $date= $date->format('d / m / Y');
		$author = get_the_author_meta( 'nickname', $post->post_author );
		$title = $post->post_title;
		$extract = $post->post_excerpt;
		$content= apply_filters('the_content',$post->post_content);
		$categories = get_the_category($post->ID);
		$link= $post->post_name; 
?>			<div class="article_title"><?php echo $title; ?></div>
			<div class="article_date"><?php echo $date; ?></div>
			<div class="article_author"><span>Author: </span><?php echo $author; ?></div>
			<div class="article_extract"><?php echo $extract; ?></div>
			
			<div class="article_content">
				<!--div class="article_img"><?php echo $image==''? '<span class="nada"></span>': $image; ?></div-->
			<?php echo $content; ?></div>
            <div class="comments-gast">
            <?php
			  // If comments are open or we have at least one comment, load up the comment template.
			  if ( comments_open() || get_comments_number() ) :
			  comments_template();
			  endif;
			?>
            </div>
            
            <!-- Fin Articulto -->
		<?php
		// ----------- FIN ARTICULO		
		
		
	}else {
	// *********** CATEGORIA

		$post_per_page = 4;
		$offset=isset($_GET['offset'])? (int)$_GET['offset']: 0;
		
		$query_array=array(
			'posts_per_page'   => $post_per_page,
			'offset'           => $offset*$post_per_page,
			'category_name'    => $category_slug,
			'orderby'          => 'post_date',
			'order'            => 'DESC',
			'post_type'			=> 'post',
			'post_status'		=> 'publish'
		);
		if($isAuthor){ $query_array['author']=$author->ID; ?><!-- ASOC:[<?php echo $author->ID;	?>]--><?php }
		$query = new WP_Query($query_array);						
		$posts_array=$query->posts;
		$max_num_post = $query->max_num_pages; 
?>		
		<ul class="list_post">
<?php	foreach($posts_array as $post){
			$image=get_the_post_thumbnail( $post->ID ); $image= $image!=''? $image: '<span class="noImage"></span>';
			$date = new DateTime($post->post_date); $date= $date->format('d / m / Y');
			$author = get_the_author_meta( 'nickname', $post->post_author );
			$title = $post->post_title;
			$extract = $post->post_excerpt;
			$content= $post->post_content;
			$categories = get_the_category($post->ID);
			$link= '/articulos-culinarios/'.$categories[0]->slug.'/'.$post->post_name; ?>
			<li class="article">
				<div class="img"><?php echo $image==''? '<span class="nada"></span>': $image; ?></div>
				<div class="match">
					<div class="date"><?php echo $date; ?></div>
					<div class="author"><span class="label">Autor:</span><?php echo $author; ?></div>
					<div class="title"><?php echo $title; ?></div>
					<div class="extract"><?php echo $extract; ?></div>
					<a class="link" href="<?php echo $link; ?>">Ver más</a>
					<div class="clearBoth"></div>
				</div>
				<div class="clearBoth"></div>
			</li>
            
            
			<?php
		}
		if(count($posts_array)==0){ ?>No se encontraron artículos en está sección.<?php }
		?>
		</ul>
<?php 
	$category_url=$isCategory? $urlCaerpeta.$category_slug: '/articulos-culinarios';
	$category_and_offset_prev= $offset==0? '#': $category_url.'?offset='.($offset-1);
	$_prev_class= $offset==0? 'disabled': '';
	$category_and_offset_next= $max_num_post-1==$offset? '#': $category_url.'?offset='.($offset+1);
	$_next_class= $max_num_post-1==$offset? 'disabled': '';
?>		<div class="paginador">
			<div class="label">Pagina <?php echo $offset+1; ?> de <?php echo $max_num_post; ?></div>
			<div class="nav">
				<a class="prev <?php echo $_prev_class; ?>" href="<?php echo $category_and_offset_prev ?>" >Anterior</a>
				<a class="next <?php echo $_next_class; ?>" href="<?php echo $category_and_offset_next ?>" >Siguiente</a>
				<div class="paginator">
				<?php 
				$margin=3; $pos_a=$margin; $pos_b=$offset-$margin+1; $pos_c=$offset+$margin; $pos_d=$max_num_post-$margin;
				for($i=0; $i<$max_num_post; $i++){
					if($pos_a<$pos_b and $i>=$pos_a and $i<$pos_b ){ $i=$pos_b; ?><span>...</span><?php }
					if($pos_c<$pos_d and $i>=$pos_c and $i<$pos_d ){ $i=$pos_d; ?><span>...</span><?php }
					if($i==$offset){
						?><b><?php echo $i+1; ?></b><?php
					}else{
						?><a href="<?php echo $category_url.'?offset='.$i; ?>" ><?php echo $i+1; ?></a><?php
					}
				} ?>
				</div>
			</div>
		</div>
<?php 

		// ----------- FIN CATEGORIA

		if(!isset($cats)) $cats=array();

	}
?>					</div>
					<ul class="submenu">
						<div class="title">Ver artículos de:</div>
<?php					$categories=get_categories(array(
							'type'                     => 'post',
							'child_of'                 => 0,
							'parent'                   => '',
							'orderby'                  => 'ID',
							'order'                    => 'ASC',
							'hide_empty'               => 0,
							'hierarchical'             => 1,
							'exclude'                  => '',
							'include'                  => '',
							'number'                   => '',
							'taxonomy'                 => 'category',
							'pad_counts'               => false 

						));
						foreach($categories as $cat){
							$class = $category_slug==$cat->slug? 'active' :'' ;
							$class.=' '.(in_array($cat->term_id, $cats)? 'in ': '');	?>
							<li class="<?php echo $class; ?>">
								<a href="<?php echo  '/articulos-culinarios/'.$cat->slug; ?>"><?php echo $cat->name; ?></a>
							</li><?php
						}
?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	