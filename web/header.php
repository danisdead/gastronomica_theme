<!DOCTYPE html>
<html lang="es-MX">
	<head>
		<!--meta name="viewport" content="width=device-width, user-scalable=no" /-->
		<meta name="viewport" content="width=1024, user-scalable=no">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/ios/fav-01.png" />
  		<link rel="apple-touch-icon-precomposed" href="<?php bloginfo('template_url'); ?>/ios/fav-02.png" />
		<title>Gastronómica Internacional</title>
		<?php
				wp_head();			
		?>
		<meta property="og:title" content="<?php echo get_the_title(); ?>" />
		<meta property="og:site_name" content="Gastronómica Internacional" />
		<meta property="og:url" content="<?php echo get_permalink(); ?>" />
		<meta property="og:description" content="" />
		<meta property="og:image" content="<?php echo  wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) ) ; ?>" />		

		<script  src="<?php bloginfo('template_url'); ?>/js/jquery-1.11.2.min.js"></script>
		<script> $.noConflict(); $=jQuery;</script>
		<!-- css3-mediaqueries.js for IE less than 9 -->
		<!--[if lt IE 9]><script src="//css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
		
	<style>	
		html { margin-top: 0px !important; }
	* html body { margin-top: 0px !important; }
	@media screen and ( max-width: 782px ) {
		html { margin-top: 0px !important; }
		* html body { margin-top: 0px !important; }
	}
	</style>

		
		<!-- jPaler -->
		<link type="text/css" href="<?php bloginfo('template_url'); ?>/plugins/jPlayer/css/jplayer.blue.monday.css" rel="stylesheet" />
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/plugins/jPlayer/jquery.jplayer.min.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/plugins/jPlayer/jplayer.js"></script>
	
		
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.placeholder.js"></script>
		
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/web/js/colorbox/colorbox.css">
		<script src="<?php bloginfo('template_url'); ?>/web/js/colorbox/jquery.colorbox.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/web/js/superscrollorama/greensock/TweenMax.min.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/web/js/superscrollorama/jquery.superscrollorama.js"></script>

		<script src="<?php bloginfo('template_url'); ?>/js/jquery.smpl_slides.js"></script>
		
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/flags/flags.css">

		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/web/css/style.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/web/css/home.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/web/css/header.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/web/css/footer.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/web/css/menu.css">
		<script src="<?php bloginfo('template_url'); ?>/web/js/menu.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/web/js/page.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/web/js/share.js"></script>
		
		<!--- ANALYTCIS --->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-46606503-1', 'auto');
		  ga('require', 'displayfeatures');
		  ga('send', 'pageview');

		</script>
		<!--- FIN ANALYTICS --->
		
                <!--Start of Zopim Live Chat Script-->
                <script type="text/javascript">
                 window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
                 d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
                 _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
                 $.src="//v2.zopim.com/?3BGT8oFDBBm0oecNp9E0iBhFcd4Valw2";z.t=+new Date;$.
                 type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
                </script>
                <!--End of Zopim Live Chat Script-->
				
			<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/web/css/ie9.css" />
		
	</head>
	<body>
                <div id="wrapper">
			<div id="header">
				<h1><a href="/">Gastronómica<span>Internacional</span></a></h1>
				<p>Instituto de formación online</p>
				<div class="main_menu">

<?php /*
<h2 ><?php _e('Categories'); ?></h2>
<ul > 
<?php wp_list_cats('sort_column=name&optioncount=1&hierarchical=0'); ?>
</ul>
<h2 ><?php _e('Archives'); ?></h2>
<ul >
<?php wp_get_archives('type=monthly'); ?>
</ul>
</div> */?>


<?php 
	//wp_page_menu( $args );
	//echo "<pre style='text-align:left;'>"; print_r( get_pages(  )); echo "</pre>";
	//echo "-<pre style='text-align:left;'>"; print_r( get_post_custom(2)['horales']); echo "</pre>-";
	require('menu.php'); 
?> 

				</div>
				
				
				
				<div class="fast_contact">
					<div class="fast_contact_content">
						<div class="fast_contact_menu_button"><span>Más Informes</span></div>
						<div class="fast_contact_title">Solicita más</br>informes aquí</div>
						<form method="POST" >
							<div class="field field_name">
								<label for="field_name" >Nombre completo</label>
								<input type="text" id="field_name" name="field_name" placeholder="Nombre completo" data-expreg="[A-Za-z ÑñÁáÉéÍíÓóÚúü]{3,}" data-only="[A-Za-z ÑñÁáÉéÍíÓóÚúü]" />
								<span class="error">Indique su nombre completo.</span>
							</div>
							<div class="field field_email">
								<label for="field_email" >Correo electrónico</label>
								<input type="text" id="field_email" name="field_email" placeholder="Correo electrónico" data-expreg="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9_-]+\.[a-zA-Z0-9-.]+$" data-only="[a-zA-Z0-9\.\-_@]" />
								<span class="error">Correo electrónico inválido.</span>
							</div>
							<div class="field field_telefono">
								<label for="field_telefono" >Teléfono</label>
								<input type="text" id="field_telefono" name="field_telefono" placeholder="Teléfono" data-expreg="^([0-9]{7,15})$" maxlength="15" data-only="[0-9]" />
								<span class="error">Indique un teléfono completo.</span>
								<input type="hidden" id="field_lada" name="field_lada" placeholder="Lada" value="52" />
								<div id="ladaSelector">
									<div class="current"><span class="ico"></span><span class="title"></span></div>
									<ul>
										<li class="mex" data-val="52"><span class="ico"></span><span class="title">México +52</span></li>
										<li class="usa" data-val="1"><span class="ico"></span><span class="title">Estados Unidos +1</span></li>
										<li class="esp" data-val="34"><span class="ico"></span><span class="title">España +34</span></li>
										<li class="vzl" data-val="58"><span class="ico"></span><span class="title">Venezuela +58</span></li>
										<li class="pru" data-val="51"><span class="ico"></span><span class="title">Perú +51</span></li>
										<li class="col" data-val="56"><span class="ico"></span><span class="title">Colombia +56</span></li>
										<li class="arg" data-val="54"><span class="ico"></span><span class="title">Argentina +54</span></li>
										<li class="otr"><span class="ico"></span><span class="title">Otro +</span><input type="text" id="lada_otro" value="00" maxlength="2" data-only="[0-9]" ></li>
									</ul>
								</div>
							</div>
							
							<div class="field field_phon_type">
								<input type="radio" id="field_phon_type_local" name="field_phon_type" checked="checked" value="home"/>
								<label for="field_phon_type_local" >Fijo</label>
								<input type="radio" id="field_phon_type_mbile" name="field_phon_type" value="mobile"/>
								<label for="field_phon_type_mbile" >Celular</label>
							</div>
							<div class="field field_area ieFix">
								<label for="field_area" >Area de interés</label>
								<select id="field_area" class='ieFix' name="field_area">
									<option value="No seleccionó">Área de interés</option>
									<option value="Repostería">Repostería</option>
									<option value="Cocina">Cocina</option>
									<option value="Gastronomía General">Gastronomía General</option>
									<option value="El Mundo del Vino">El Mundo del Vino</option>
									<option value="Nutrición">Nutrición</option>
									<option value="Gestión Gastronómica">Gestión Gastronómica</option>
								</select>
							</div>
							<div class="field field_questions">
								<label for="field_questions" >Escríbenos tus dudas</label>
								<textarea id="field_questions" name="field_questions" placeholder="Escríbenos tus dudas" data-expreg=".*" data-only=".*"></textarea>
							</div>
							<div class="submit submit_fast">
								<input type="hidden" name="submit_fast" value="1" />
								<input type="submit" id="submit_fast" value="Enviar Datos" />
							</div>
						</form>
						<div class="fast_contact_dropdown">&#9660;</div>
					</div>
				</div>
				  
				<!-- IFRAME RESPUESTA TRACKING  # ENAT -->
				<div id="afterFormGI" style="display: none !important; width: 0px; height: 0px !important;"> </div> 
				<!-- END IFRAME RESPUESTA TRACKING  # ENAT -->
				
			</div>
			
			<script>
			$(function(){
				var fast_contact=$('.fast_contact');
				var btn_submit=$('#submit_fast',fast_contact);
				var fields_exp=$('[data-expreg]',fast_contact);
				fields_exp.each(function(){
					$(this).change(function(){
						var me=$(this);
						var _data_expreg=me.attr('data-expreg');
						var expreg=new RegExp(_data_expreg);
						if(!expreg.test(me.val())){
							me.css({ color: 'red' });
							me.focus();
							me.siblings('.error').show();
						}else{
							me.css({ color: 'black' });
							me.siblings('.error').hide();
						}
						me.val(me.val().replace(/\s\s+/g, ' ').trim());
					});
				});
				$('[data-only]',fast_contact).on("keyup paste", function(){
					var me=$(this);
					if(me.attr('data-only')!='')
						setTimeout(jQuery.proxy(function() {
							var expreg=new RegExp(me.attr('data-only'));
							for(var i=0; i<me.val().length; i++)
								if(!expreg.test(me.val().substring(i,i+1))){
									me.val(me.val().substring(0,i)+me.val().substring(i+1)); i--;
								}
							me.val(me.val().replace(/\s\s+/g, ' '));
						}), 0);
				});
				var form=btn_submit.parents('form');
				
				var sendingFormInProcess = "NO"; // FLAG @NATHALY
				
				form.submit(function(){
					fields_exp.each(function(){ $(this).change(); });
					if( $('span.error:visible').length==0 && sendingFormInProcess == "NO" ){
						
						sendingFormInProcess = "SI";
						var width=btn_submit.parent().width();
						var interval;
						btn_submit.css('text-indent',-1000).animate({paddingTop:0,paddingBottom:0},500,function(){
							btn_submit.css({left:-width,backgroundColor:'#ff6d0d'});
							var i=0;
							interval=setInterval(function(){ i++; btn_submit.stop().animate({left:~~(-width/i)}); },100);
						});
						$.post('/',form.serialize(),function(data){
							setTimeout(function(){
								clearInterval(interval);
								if(data=='1') {
									form[0].reset(); $('input, textarea',form).placeholder();
									// la siguiente línea envía el tracking de Facebook
									// window._fbq.push(['track', '6014697592748', {'value':'0.00','currency':'USD'}]);
									
									// IFRAME CON TRACKING PAGE : ENAT
									if(document.getElementById("afterFormGI")) {
										document.getElementById("afterFormGI").innerHTML = "<iframe src='/gitracking.php' style='display: none !important;' ></iframe>"	;
									}
								}
								btn_submit.stop().css({left:0}).addClass(data=='1'?'true':'false')
									.animate({paddingTop:8,paddingBottom:8},500, function(){
										btn_submit.stop().animate({opacity:1},500, function(){
											setTimeout(function(){
												btn_submit.stop().animate({opacity:0},500, function(){
													btn_submit.stop().css({ textIndent:0, backgroundColor:''}).removeClass('true').removeClass('false')
													.animate({opacity:1},500, function(){ $('.fast_contact_dropdown').click(); });
												});
											},1000);
										});
									});
								sendingFormInProcess = "NO";	
							},500)
							
						});
					}
					return false;
				});
				
				$('input, textarea',form).placeholder();
				

				$('#field_area').change(function(){
					$(this).css({ color: ($(this).find(':selected').index()==0?'#aaa':'#000') });
				}).focus(function(){
					$(this).css({color:'#000'});
				}).blur(function(){
					$(this).css({ color: ($(this).find(':selected').index()==0?'#aaa':'#000') });
				}).css({color:'#aaa'});
			});
			</script>