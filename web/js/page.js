// **** Oferta educativa ****

// **** Nivel 2 - Sub menu lateral
/*
$(function(){
	var menu=$('#level2_linkajax')
	var content=$('#ajaxable_content');
	var links=menu.find('a');
	var i=window.location.toString().split('#');
	i[1]= (i[1] || '');
	var current=links.filter(':contains("'+i[1]+'")');
	if(current.length==0 && i==''){ current=links.first(); }
		$.getJSON(current.attr('data-href'),function(data){
			links.removeClass('active');
			current.addClass('active');
			content.empty();
			content.html(data.post_content);
			loadDocenteLinks();
		});
	
})*/


function loadDocenteLinks(){
	$('[data-urldocente]').each(function(){
		var me=$(this); 
		$.getJSON(me.attr('data-urldocente'),function(data){
			if(data instanceof Object)
			me.html(
											
				'<div class="image"><img src="'+( (data.vars && data.vars.image)? data.vars.image.join(' '): '') +'">'+
					'<div class="over">Conoce su trayectoria<a href="'+me.attr('data-urldocente')+'">Ver</a></div></div>'+
				'<div class="name">'+( (data.post_title && data.post_title)? data.post_title: '') +'</div>'+
				'<div class="title">'+( (data.vars && data.vars.title)? data.vars.title.join(' '): '') +'</div>'+
				'<div class="countries"><span class="f_'+( (data.vars && data.vars.countries)? data.vars.countries.join('').split(',').join('"></span><span class="f_'): '')+'"></span>'
			);
		});
	});
}



$(function(){
	
	$('.gastromath .a, .gastromath .b, .gastromath .c, .gastromath .a2, .gastromath .b2, .gastromath .c2, .gastromath .plus, .gastromath .equal')
	.each(function(){
		var me=$(this);
		var text=me.html();
		me.html('<span>'+ text+'</span>');
	});
	loadDocenteLinks();
	
	
	$('a.colorbox').colorbox({rel:'gal'});
});