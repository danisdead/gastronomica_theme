/*
$(document).ready(function(){
	// estabilizador de scroll
	// cunado se escrolea busca centrar los slide
	var home=$('#home');
	var slides=$('.home-slide',home);
	var timeOutForCenterSlides;
	var main=$(window);
	main.scroll(function(){
		main.stop();
		if(timeOutForCenterSlides) clearTimeout(timeOutForCenterSlides);
		timeOutForCenterSlides=setTimeout(function(){
			var scrollTop=main.scrollTop(), distance;
			var window_height_2=[window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight]/2;
			for(var i=0; i<slides.length; i++){
				distance=scrollTop+window_height_2-(slides.eq(i).offset().top+slides.eq(i).height()/2);
				//console.log(distance);
				if(scrollTop>0 && distance<100){
					//$(main).stop().animate({scrollTop: scrollTop-distance}, 1000);
					$('html, body').animate({scrollTop: scrollTop-distance}, 200);
					break;
				}
			}
		},500);
	});
})

*/


jQuery(function(){ 
	$('.smpl_hslide').each(function(){
		var dataClass=$(this).attr('class');
		switch(dataClass){
			default: $(this).smpl_hslide(); break;
			case 'smpl_hslide slideBackground':
			case 'smpl_hslide slideBackground loscursos':
				var fn_in=function(slide_out, slide_in, fn_out, origin){
						var _text_out=$('.slide-mark',slide_out), _img_out=$('.page-image',slide_out),
							_text_in=$('.slide-mark',slide_in), _img_in=$('.page-image',slide_in);
						_text_out.stop().animate({opacity:0,left:-40},250,function(){
							_img_out.stop().animate({left:-_img_out.width()},500, function(){ slide_out.hide(); });
							slide_in.show();
							_text_in.css({ opacity:0,left:40 });
							_img_in.css({left:_img_in.width()*2}).stop().animate({left:0},500,function(){
								_text_in.stop().animate({ opacity:1,left:0 },250);
								fn_out(slide_in, origin);
							});
						});
						
					},
					fn_out=function(slide_in, origin){
						origin.update();
					},
					fn_change=function(){},
					fn_start=function(origin){
						
					};
				$(this).smpl_hslide(fn_in,fn_out,fn_change,fn_start);
			//	$('.next',this).click();
				break;
			case 'smpl_hslide slideImage':
				var fnMod=function(x,len){ return x>=len? x-len: (x<0? x+len: x); };
				var fn_in=function(slide_out, slide_in, fn_out, origin){
						var allSlides=origin.find('div.slide');
						var _in=allSlides.index(slide_in);
						var _out=allSlides.index(slide_out);
						var x_0, x_1, x_2, dir=0;
							if	( _in==_out+1 || (_in<_out && _out!=_in+1) ){ dir= 1, x_0=fnMod(_in-2,allSlides.length), x_1=fnMod(_in-1,allSlides.length), x_2=fnMod(_in,allSlides.length); }
						else if	( _in==_out-1 || (_in>_out && _out!=_in-1) ){ dir=-1, x_0=fnMod(_in,allSlides.length), x_1=fnMod(_in+1,allSlides.length), x_2=fnMod(_in+2,allSlides.length); }
						else { fn_out(slide_in, origin); return false; }
						allSlides.each(function(){ $(this).children().hide(); });
						$(slide_in).children().show();
						if(dir==1){
							$('.img',allSlides.eq(x_0)).show().stop().css({ left:-400, top:110, width:256,height:352 }).animate({ left:105, top:110, width:256,height:352 },1000).children('img').css({opacity:0.7}).parents('.slide').css({zIndex:1});
							$('.img',allSlides.eq(x_1)).show().stop().css({ left:-160, top:0, width:320,height:440 }).animate({ left:-400, top:110, width:256,height:352 },1000).children('img').css({opacity:1}).animate({opacity:0.7},1000).parents('.slide').css({zIndex:2});
							$('.img',allSlides.eq(x_2)).show().stop().css({ left:105, top:110, width:256,height:352 }).animate({ left:-160, top:0, width:320,height:440 },1000).children('img').css({opacity:0.7}).animate({opacity:1},1000,function(){ fn_out(slide_in, origin); }).parents('.slide').css({zIndex:3});
						}else{
							$('.img',allSlides.eq(x_0)).show().stop().css({ left:-400, top:110, width:256,height:352 }).animate({ left:-160, top:0, width:320,height:440 },1000).children('img').css({opacity:0.7}).animate({opacity:1},1000,function(){ fn_out(slide_in, origin); }).parents('.slide').css({zIndex:3});
							$('.img',allSlides.eq(x_1)).show().stop().css({ left:-160, top:0, width:320,height:440 }).animate({ left:105, top:110, width:256,height:352 },1000).children('img').css({opacity:1}).animate({opacity:0.7},1000).parents('.slide').css({zIndex:2});
							$('.img',allSlides.eq(x_2)).show().stop().css({ left:105, top:110, width:256,height:352 }).animate({ left:-400, top:110, width:256,height:352 },1000).children('img').css({opacity:0.7}).parents('.slide').css({zIndex:1});
						}
						
					},
					fn_out=function(slide_in, origin){
						//alert();
						origin.update();
					},
					fn_change=function(){},
					fn_start=function(origin){
						var slides=origin.find('div.slide');
						slides.show();
						//slides.each(function(){ var me=$('.img img', this); me.parent().css('background','url('+me.attr('src')+')'); })
					};
				$(this).smpl_hslide(fn_in,fn_out,fn_change,fn_start);
			//	$('.next',this).click();
				break;
		}
	});
});



$(function(){
	
	var tabs=$('.smpl_tabs .tab');
	tabs.mouseenter(function(){
		var _cntnt=$('.tab-content',this), _link=$('.link',this), _img=$('img',this);
		_img.stop().css({ opacity:1 }).animate({ opacity:0.2 },500);
		var pos=_link.position();
		_link.css({ opacity:0, top:-45 });
		_cntnt.stop().css({ opacity:0 }).animate({ opacity:1 },300, function(){
			_link.stop().animate({ opacity:1, top:-50 },200);
		});
	});
	tabs.mouseleave(function(){
		var _cntnt=$('.tab-content',this), _link=$('.link',this), _img=$('img',this);
		_img.stop().css({ opacity:0.2 }).animate({ opacity:1 },500);
	});
	tabs.click(function(){
		window.location=$(this).find('a').attr('href');
	});
	
});



$(function(){
	
	
	var controller = $.superscrollorama();
	controller.addTween('.home-slide_1', TweenMax.from( $('.home-slide_1 .home-slide_mainContainer .tab:eq(0)'), 0.5+Math.random()*0.5, {css:{opacity:0, top:100+Math.random()*50}}));
	controller.addTween('.home-slide_1', TweenMax.from( $('.home-slide_1 .home-slide_mainContainer .tab:eq(1)'), 0.5+Math.random()*0.5, {css:{opacity:0, top:100+Math.random()*50}}));
	controller.addTween('.home-slide_1', TweenMax.from( $('.home-slide_1 .home-slide_mainContainer .tab:eq(2)'), 0.5+Math.random()*0.5, {css:{opacity:0, top:100+Math.random()*50}}));
	controller.addTween('.home-slide_1', TweenMax.from( $('.home-slide_1 .home-slide_mainContainer .tab:eq(3)'), 0.5+Math.random()*0.5, {css:{opacity:0, top:100+Math.random()*50}}));
	controller.addTween('.home-slide_1', TweenMax.from( $('#home .home-slide_mainContainer .home-slide-content'), 0.5+Math.random()*0.5, {css:{opacity:0}}));
	//controller.addTween('.home-slide_2', TweenMax.from( $('.home-slide_2 .slide-content'), 1.2, {css:{top:-50, opacity:0}}));
	//controller.addTween('.home-slide_2', TweenMax.from( $('.home-slide_2 .link'), 1.2, {css:{top:450, opacity:0}}));
	//controller.addTween('.home-slide_2', TweenMax.from( $('.home-slide_2 .slide_nav'), 0.8, {css:{opacity:0}}));
	//controller.addTween('.home-slide_3', TweenMax.from( $('.home-slide_3 .slide'), .8, {css:{top:100}}));
	
});

$(function(){
	var _slide2prev=$('.home-slide_2 .prev, .home-slide_3 .prev'), _slide2next=$('.home-slide_2 .next, .home-slide_3 .next');
	 _slide2prev.add(_slide2next).css({position:'relative'});
	var move=10, time=500;
	var ani=function(){
		_slide2prev.stop().css({left:-move}).animate({left:move},time);
		_slide2next.stop().css({left:move}).animate({left:-move},time, function(){
			_slide2prev.stop().css({left:move}).animate({left:-move},time);
			_slide2next.stop().css({left:-move}).animate({left:move},time, function(){ ani(); });
				
		});
	}
	ani();
});

$(function(){
	var scrollder=false;
	var $html_body=$('html, body');
	var header=$('#header');
	var hslides=$('.home-slide').add('#footer');
	var nav=$('.home-slide_mainNavigator');
	var navs=$('div',nav);
	navs.click(function(){
		var me=$(this);
		me.addClass('active').siblings().removeClass('active');
		var pos=navs.index(me);
		var current=hslides.eq(pos);
		var top=current.offset().top-100+42;//+current.height()/2-$html_body.height()/2;
		$html_body.stop().animate({scrollTop: top}, 1000, function(){ scrollder=false; });
		scrollder=true;
	});

	var main=$(window);
	main.scroll(function(){
		if(!scrollder){
			var scrollTop=main.scrollTop();
			hslides.each(function(i){
				var me=$(this);
				var h=me.offset().top;//+me.height();
				if(h-scrollTop>=0){
					navs.eq(i).addClass('active').siblings().removeClass('active'); return false;
				}
			});
		}
	});
});
$( document ).on( "keydown", function( event ) {
	var e=event || window.event;
	var key =e.which || e.keyCode;
	var nav=$('.home-slide_mainNavigator');
	var navs=$('div',nav);
	if(key==33){ navs.filter('.active').prev().click(); return false; }
	if(key==34){ navs.filter('.active').next().click(); return false; }
});


$(window).resize(function(){
	var main_menu=$('#header .main_menu');
	var altura=$(window).height();//-main_menu.height()*1;
	// slides background
	$('.slideBackground').height(altura);
	$('.slideBackground .page-image').height(altura);
	$('.slideBackground:eq(1)').height($(window).height()*1.2/2);
	$('.slideBackground:eq(1) .page-image').height($(window).height()*1.2/2);
	$('.slideBackground:eq(1) .slide-mark').css('top',($(window).height()*1.2/2)/2 - 250/2); //lo centra
	$('.slideBackground:eq(1) .slide_nav').css('top',($(window).height()*1.2/2)/2 - 92/2 +20); //lo centra
	// tabs
	$('.smpl_tabs').css({ width:'100%', height:($(window).width()<1000?1000:$(window).width())*363/1280 });
	// inicia el roller de cocineros inferior. 4to slide
	$('.home-slide_3 .next').click();
});
$(function(){ $(window).resize(); });