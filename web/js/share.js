var sendGA_form;
$(function(){
	
	// share facebook
	$('.facebook').click(function(){
		ga('send', 'event', {
			eventCategory: 'facebook', 
			eventAction: 'lead'
		});
		setTimeout(function(){
			ga('send', {
				'hitType': 'social',
				'socialNetwork': 'facebook',
				'socialAction': 'share'
			});
		},500);
	});
	// link youtube
	$('.youtube').click(function(){
		ga('send', 'event', {
			eventCategory: 'youtube', 
			eventAction: 'lead'
		});
		setTimeout(function(){
			ga('send', {
				'hitType': 'social',
				'socialNetwork': 'youtube',
				'socialAction': 'lead'
			});
		},500);
	});
	// link instragram
	$('.instagram').click(function(){
		ga('send', 'event', {
			eventCategory: 'instagram', 
			eventAction: 'lead'
		});
		setTimeout(function(){
			ga('send', {
				'hitType': 'social',
				'socialNetwork': 'instagram',
				'socialAction': 'lead'
			});
		},500);
	});
	// link twitter
	$('.twitter').click(function(){
		ga('send', 'event', {
			eventCategory: 'twitter', 
			eventAction: 'lead'
		});
		setTimeout(function(){
			ga('send', {
				'hitType': 'social',
				'socialNetwork': 'twitter',
				'socialAction': 'lead'
			});
		},500);
	});


	var contriesByLada={
		52:'Mexico',
		1:'United States',
		34:'España',
		58:'Venezuela',
		51:'Peru',
		56:'Colombia',
		54:'Argentina'
	};
	// form submit
	$('#submit_fast, #_submit_fast').each(function(){
		$(this).parents('form').submit(function(){
			var area=$(this).find('[name="field_area"]').val();
			var lada=parseInt($(this).find('[name="field_lada"]').val());
			ga('send', 'event', {
				eventCategory: 'solicitud_de_informacion', 
				eventAction: area,
				eventLabel: (contriesByLada[lada]!=undefined?contriesByLada[lada]:lada)
			});
		});
	});
	
	
	// campo vistual
	$('.menu_campus_virtual a').click(function(){
		ga('send', 'event', {
		eventCategory: 'outbound', 
		eventAction: 'campus_virtual'
		});
	});
	
	
	// aviso de privacidad
/*	$('.politicas').click(function(){
		ga('send', 'event', {
		eventCategory: 'outbound', 
		eventAction: 'aviso_de_privacidad'
		});
	});
	
	// terminos y condiciones
	$('.terminos').click(function(){
		ga('send', {
		  'hitType': 'pageview',
		  'page': '/terminos_y_condiciones',
		  'title': 'Términos y condiciones'
		});
	}); */
	
	// oferta educativa
	var menu=$('#level2_linkajax')
	var content=$('#ajaxable_content');
	var links=menu.find('a');
	links.click(function(){
		ga('send', {
		  'hitType': 'pageview',
		  'page': '/'+window.location.toString().split('/').slice(3).join('/')+encodeURIComponent($('span',this).text()),
		  'title': document.title
		});
	});
	
	activateDownloadGA=function() {
		$('[download]').click(function(){
			var me=$(this); me.attr('target','_blank');
			ga('send', 'event', {
				eventCategory: 'descarga_de_archivo', 
				eventAction: me.attr('download'),
				eventLabel: me.attr('href')
			});
		});
	}
	$( document ).ajaxComplete(activateDownloadGA);
	activateDownloadGA();
	
	// download
	
});