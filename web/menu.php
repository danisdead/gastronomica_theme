<?php
// Requerido por header.php


// funcion que dibuja el men�
function createMenu($parent=0, $level=0){
	if($level>=3) return false;
	// toma el listado de p�ginas
	$pages = get_pages( array('parent' => $parent, 'sort_order' => 'ASC', 'sort_column' => 'menu_order' ) );
	$len = count($pages);
	if($len>0){
		// dibuja la lista del menu ?>
		<ul class="<?php echo 'level_'.$level; ?>"><?php
		$last_index=$len-1; // indice del �ltimo elemento del nivel
		$active_id=get_the_ID(); // id de la p�gina activa
		$i=0;
		// recorre la lista de p�ginas
		foreach($pages as $page){
			// tova las variables configurables de c�da p�gia
			$vars=get_post_custom($page->ID);
			// agrega las clases de estilo
			$clases=array();
				$clases[]='level_'.$level;
				$clases[]= $i==0? 'first': ( $i==$last_index? 'last': 'middle');
				//$clases[]= ($i%2)==0? 'par': 'non';
				if($active_id == $page->ID) $clases[]='active';
				if(isset($vars['link-class'])) $clases[]=implode(' ',$vars['link-class']);
			// direcci�n final
			$href=isset($vars['link-href'])? current($vars['link-href']): get_permalink( $page->ID );
			$target=isset($vars['link-target'])? current($vars['link-target']): '_self';
			// dibuja la liga ?>
			<li class="<?php echo implode(' ',$clases); ?>">
				<a href="<?php echo $href; ?>" target="<?php echo $target; ?>"><span><?php echo isset($vars['link-title'])?$vars['link-title'][0]:$page->post_title; ?></span></a><?php
				// dubuja los hijos
				createMenu($page->ID, $level+1); ?>
			</li><?php
			$i++; // incremente i, para llevar la relacion de primero y ultimo, par y non
		}
		// dibuja el fin del menu ?>
			<li class="clearboth"></li>
		</ul><?php
	}
}

// carga las opciones de la plantilla
$options=get_option('pa_opciones');

?>
		<!---- MAIN MENU ----->
		<div class="menu_topbar">
			<div class="menu_topbar_container">
				<div class="telephon">
				
					<span class="phon"><span>+52</span> <span>(55)</span> <span>5535</span> <span>1728</span></span>
					<span class="phon"><span>+52</span> <span>(55)</span> <span>5535</span> <span>2563</span></span>
										
				</div>
				<div class="menu_campus_virtual">
					<a target="_blank" href="http://gastronomicainternacional.we-know.net/login/index.php"><span>Campus Virtual</span></a>
				</div>
			</div>
		</div><?php


?>
	<div id="main_menu">
		<?php createMenu(0); ?>
		<div class="clearBoth"></div>
	</div><?php



/*
echo "<pre style='text-align:left;'>"; print_r( get_pages( array('parent' => 0) ) ); echo "</pre>";
echo "-<pre style='text-align:left;'>"; print_r( get_post_custom(2)['horales']); echo "</pre>-";
*/