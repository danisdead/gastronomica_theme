<?php


// carga las opciones de la plantilla
$options=get_option('pa_opciones');

	// es categoría o es un artículo
	$isCategory=(isset($_GET['category_slug']) && $_GET['category_slug']!='')?true:false;
	$isPost=(isset($_GET['post_slug']) && $_GET['post_slug']!='')?true:false;

	// carga la categoría
	$category_slug = $isCategory? $_GET['category_slug']: '';
	$category = get_category_by_slug( $category_slug );

	// carga el artículo
	$post_slug = $isPost? $_GET['post_slug']: '';
	$args=array(
	  'name' => $post_slug,
	  'post_type' => 'post',
	  'post_status' => 'publish',
	  'numberposts' => 1
	);
	$posts = get_posts($args);
	
// variables principales
$title= $isPost? $posts[0]->post_title: (is_object($category)? $category->name: 'Artículos Culinarios');
$description = $isPost?
	'Categoría: '.(is_object($category)? $category->name: '')
	: 'Conoce lo último en tendencias en nuestros artículos.'; 
?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/web/css/articulosculinarios.css">

	<div id="primary" class="content-area">
		<div id="pagecontent" class="site-content" role="main">
			
            
            <!-- Category -->
            <ol vocab="http://schema.org/" typeof="BreadcrumbList" class="breadcrumbs">
            	<li property="itemListElement" typeof="ListItem">
            		<a property="item" typeof="WebPage" href="<?php echo get_site_url(); ?>" >
                    <span property="name">HOME</span>
                    </a>
                    <meta property="position" content="1">
                    <span>&gt;</span>
				</li>
                <li property="itemListElement" typeof="ListItem">
            		<a property="item" typeof="WebPage" href="<?php echo get_site_url(); ?>/articulos-culinarios" >
                    <span property="name">Artículos Culinarios</span>
                    </a>
                    <meta property="position" content="2">
                    <span>&gt;</span>
				</li>
				<?php 
					$the_cat = get_the_category();
					$category_name = $the_cat[0]->cat_name;
					$category_link = get_category_link( $the_cat[0]->cat_slug ); 
				?>
				
                <li property="itemListElement" typeof="ListItem">
            		<a property="item" typeof="WebPage" href="<?php echo $category_link ; ?>" class="active">
                    <span property="name"><?php echo $category->name; ?></span>
                    </a>
                    <meta property="position" content="3">
                    <span>&gt;</span>
				</li>
            </ol>
            
            
            
            
			<div class="articulosculinarios">
				<div class="nivel2">
					<div class="articulosculinarios_title"><span><?php echo $title; ?></span></div>
					<div class="articulosculinarios_content"><?php echo $description; ?></div>
					<div class="content">
<?php
	if($isPost){
	// *********** ARTICULO 

		// 
		$post= $posts[0];
		
		$image=get_the_post_thumbnail( $post->ID );
		$date = new DateTime($post->post_date); $date= $date->format('d / m / Y');
		$author = get_the_author_meta( 'nickname', $post->post_author );
		$title = $post->post_title;
		$extract = $post->post_excerpt;
		$content= $post->post_content;
		$categories = get_the_category($post->ID);
		$link= '/articulos-culinarios/'.$categories[0]->slug.'/'.$post->post_name; 
?>			<div class="article_title"><?php echo $title; ?></div>
			<div class="article_date"><?php echo $date; ?></div>
			<div class="article_author"><span>Author: </span><?php echo $author; ?></div>
			<div class="article_extract"><?php echo $extract; ?></div>
			<div class="article_img"><?php echo $image==''? '<span class="nada"></span>': $image; ?></div>
			<div class="article_content"><?php echo $content; ?></div>
		<?php
		// ----------- FIN CATEGORIA		
		
		
	}else {
	// *********** CATEGORIA

		$post_per_page = 4;
		$offset=isset($_GET['offset'])? (int)$_GET['offset']: 0;
		
		$query = new WP_Query(array(
				'posts_per_page'   => $post_per_page,
				'offset'           => $offset,
				'category_name'    => $category_slug,
				'orderby'          => 'post_date',
				'order'            => 'DESC',
				'post_type'			=> 'post',
				'post_status'		=> 'publish'
			));						
		$posts_array=$query->posts;
		$max_num_post = $query->max_num_pages; 
?>		
		<ul class="list_post">
<?php	foreach($posts_array as $post){
			$image=get_the_post_thumbnail( $post->ID ); $image= $image!=''? $image: '<span class="noImage"></span>';
			$date = new DateTime($post->post_date); $date= $date->format('d / m / Y');
			$author = get_the_author_meta( 'nickname', $post->post_author );
			$title = $post->post_title;
			$extract = $post->post_excerpt;
			$content= $post->post_content;
			$categories = get_the_category($post->ID);
			$link= '/articulos-culinarios/'.$categories[0]->slug.'/'.$post->post_name; ?>
			<li class="article">
				<div class="img"><?php echo $image==''? '<span class="nada"></span>': $image; ?></div>
				<div class="date"><?php echo $date; ?></div>
				<div class="author"><span class="label">Autor:</span><?php echo $author; ?></div>
				<div class="title"><?php echo $title; ?></div>
				<div class="extract"><?php echo $extract; ?></div>
				<a class="link" href="<?php echo $link; ?>">Ver más</a>
				<div class="clearBoth"></div>
			</li>
			<?php
		} ?>
		</ul>
<?php 
	$category_url=$isCategory? '/articulos-culinarios/'.$category_slug: '/';
	$category_and_offset_prev= $offset==0? '#': $category_url.'?offset='.($offset-1);
	$_prev_class= $offset==0? 'disabled': '';
	$category_and_offset_next= (int)($max_num_post/$post_per_page)==$offset? '#': $category_url.'?offset='.($offset+1);
	$_next_class= (int)($max_num_post/$post_per_page)==$offset? 'disabled': '';
?>		<div class="paginador">
			<div class="label">Pagina <?php echo $offset+1; ?> de <?php echo (int)($max_num_post/$post_per_page)+1; ?></div>
			<div class="nav">
				<a class="prev <?php echo $_prev_class; ?>" href="<?php echo $category_and_offset_prev ?>" >Anterior</a>
				<a class="next <?php echo $_next_class; ?>" href="<?php echo $category_and_offset_next ?>" >Siguiente</a>
			</div>
		</div>
<?php 

		// ----------- FIN CATEGORIA



	}
?>					</div>
					<ul class="submenu">
						<div class="title">Ver artículos de:</div>
							<li class="<?php echo $isCategory? '': 'active'; ?>">
								<a href="/articulos-culinarios">Todos</a>
							</li>
<?php					$categories=get_categories(array(
							'type'                     => 'post',
							'child_of'                 => 0,
							'parent'                   => '',
							'orderby'                  => 'name',
							'order'                    => 'ASC',
							'hide_empty'               => 1,
							'hierarchical'             => 1,
							'exclude'                  => '',
							'include'                  => '',
							'number'                   => '',
							'taxonomy'                 => 'category',
							'pad_counts'               => false 

						));
						foreach($categories as $cat){
							$class = $category_slug==$cat->slug? 'active': ''; ?>
							<li class="<?php echo $class; ?>">
								<a href="<?php echo '/articulos-culinarios/'.$cat->slug; ?>"><?php echo $cat->name; ?></a>
							</li><?php
						}
?>
					</ul>
				</div>
			</div>
		</div>
	</div>
    <!-- <?php 
echo preg_replace('/\.php$/', '', __FILE__); ?>-->