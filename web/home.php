<?php

// carga las opciones de la plantilla
$options=get_option('pa_opciones');

// carga los hijos de la página HOME (sabe quién es home porque lo toma de las opciones de la plantilla)
$home_pages=array();
if(is_array($options) and isset($options['web_home_id']))
	$home_pages=get_pages(array('parent'=>$options['web_home_id'],
	'sort_order' => 'ASC',
	'sort_column' => 'menu_order'
	));

?>
<script src="<?php bloginfo('template_url'); ?>/web/js/home.js"></script>
<div id="main">
	<div id="content">
	
		<!-- home area -->
		<div id="home">
		
		
<?php
	// ***** WIDGETS ****
	foreach($home_pages as $k=>$widget){
	
?>		<div class="home-slide home-slide_<?php echo $k; ?>">
			<div class="home-slide_mainContainer">
<?php	// carga las variables de la página
		$widget_vars=get_post_custom($widget->ID);
		//  imprime titulo y contenido del widget ?>
		<h2 class="home-slide-title"><?php echo isset($widget_vars['HtmHeaderWeb'])?$widget_vars['HtmHeaderWeb'][0]: $widget->post_title ?></h2>
		<div class="home-slide-content"><?php echo $widget->post_content; ?></div><?php
		// Inerta el widget
		if(isset($widget_vars['widget-type']))
		switch($widget_vars['widget-type'][0]){
			// ****** DEFAULT
			default: echo "<pre>"; print_r($widget); echo "</pre>"; break;
			// ****** HSLIDE
			case 'hslide': ?>
				<div class="smpl_hslide <?php if(isset($widget_vars['widget-class'])) echo implode(' ',$widget_vars['widget-class']); ?>" ><?php
					// carga los slides del horizontal slide
					$slide_pages=get_pages(array('parent'=>$widget->ID,'sort_order' => 'ASC', 'sort_column' => 'menu_order'));
					if(count($slide_pages)>0)
					foreach($slide_pages as $kk=>$slide){
						$slide_vars=get_post_custom($slide->ID); ?>
					<div class="slide slide_<?php echo $kk;?>" data-icon="<?php echo $slide_vars['icon'][0]; ?>" >
						<?php if(isset($slide_vars['image'])){ ?>
						<div class="img">
							<img class="page-image" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAAEElEQVR42mL4//8/A0CAAQAI/AL+26JNFgAAAABJRU5ErkJggg=="
							style="background-image:url(<?php echo $slide_vars['image'][0]; ?>);" />
						</div><?php } ?>
						<div class="slide-mark">
							<div class="slide-title"><?php echo isset($slide_vars['title'])?$slide_vars['title'][0]:$slide->post_title; ?></div>
							<div class="slide-content"><?php echo $slide->post_content; ?></div>
							<?php if(isset($slide_vars['link-href'][0])){ ?>
							<a href="<?php echo $slide_vars['link-href'][0]; ?>" class="link"><?php echo $slide_vars['link-text'][0]; ?></a><?php } ?>
						</div>
					</div><?php
					}
					// si no hay slides en el horizontal silide lo anuncia
					else { ?><div class="admin-alert">la página no tiene páginas contenidas. Agrega páginas inferiores.</div><?php }
				?>
				</div>
			<?php break;
			case 'tabs': ?>
				<div class="smpl_tabs <?php if(isset($widget_vars['widget-class'])) echo implode(' ',$widget_vars['widget-class']); ?>"><?php
				// carga los tabs del horizontal slide
				$tabs_pages=get_pages(array('parent'=>$widget->ID,'sort_order' => 'ASC', 'sort_column' => 'menu_order'));
				if(count($tabs_pages)>0)
				foreach($tabs_pages as $tab){
					$tab_vars=get_post_custom($tab->ID); ?>
					<div class="tab">
						<?php if(isset($tab_vars['image'])){ ?>
						<div class="img"><img class="page-image" src="<?php echo $tab_vars['image'][0]; ?>" /></div><?php } ?>
						<div class="tab-title"><span><?php echo $tab->post_title; ?></span></div>
						<div class="tab-content"><?php echo $tab->post_content; ?></div>
						<?php if(isset($tab_vars['link-href'][0])){ ?>
						<a href="<?php echo $tab_vars['link-href'][0]; ?>" class="link"><?php echo $tab_vars['link-text'][0]; ?></a><?php } ?>
					</div><?php
				}
				?>
				</div>
			<?php break;
		};
?>			</div>
		</div>
<?php		
	}
?>


			<div class="home-slide_mainNavigator">
				<div class="active"></div>
				<div></div>
				<div></div>
				<div></div>
				<div></div>
			</div>

					<?php /*
					<h1>Main Area</h1>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<h1><?php the_title(); ?></h1>
					<h4>Posted on <?php the_time('F jS, Y') ?></h4>
					<p><?php the_content(__('(more...)')); ?></p>
					<hr> <?php endwhile; else: ?>
					<p><?php _e('Sorry, no posts matched your criteria.'); ?></p><?php endif; ?>
					*/ ?>



	</div>		
	
	
</div>
<div id="delimiter">
</div>

