<?php

// carga las opciones de la plantilla
$options=get_option('pa_opciones');

?>

	<div id="footer">

		<div class="partners" ><?php
		 if(is_array($options) and isset($options['web_partners_id'])){
			 $partners=get_page($options['web_partners_id']);
		?>	<div class="parners_title">Nuestros <strong>Socios</strong></div> <?php
		// ?>	<div class="parners_content"><?php echo $partners->post_content; ?></div> <?php
		} ?>
		</div>
		
		
		
		<div class="contact" >
			<div class="contact_content" >
				<div class="contact_contries">
					<div class="contry mexico">
						<div class="title">México</div>
						<div class="phone">01 (55) 5535 2563 / 1728</div>
						<div class="address">Av. Paseo de la Reforma No.155,<br>Segundo Piso,<br>Col. Cuauhtémoc, 06600,<br>Distrito Federal, México</div>
					</div><br>
					<div class="contry internet">
						<div class="title">Email</div>
						<div class="email">info@gastronomicainternacional.com</div>
						<div class="title">Skype</div>
						<div class="email">gastronomica.internacional</div>
					</div>
				</div>
				<div class="contact_botton">
					<div class="title">¿Tienes dudas?</div>
					<a href="/contacto/">Solicita información aquí</a>
				</div>
				<div class="contact_social">
					<div class="title">Síguenos</div>
					<a target="_blank" href="//www.facebook.com/GastronomicaInternacional" _href="//www.facebook.com/sharer.php?s=100&p[url]=<?php echo get_permalink(); ?>" class="facebook">facebook</a>
					<a target="_blank" href="//www.youtube.com/channel/UC9xKIOOQDEyL5d1xxYJgkOQ" class="youtube">youtube</a>
					<a target="_blank" href="//instagram.com/_gastronomica" class="instagram">instagram</a>
					<a target="_blank" href="//twitter.com/@_gastronomica" class="twitter">twitter</a>
				</div>
				<div class="clearBoth"></div>
			</div>
		</div>	
	
		<div class="footer">
			<div class="footer_content">
			
				<div class="copy">Copyright &copy; 2015, Gastronómica Internacional. Todos los derechos reservados.</div>
				<a href="<?php echo isset($options['web_piltics_id'])?get_post($options['web_piltics_id'])->guid:'#'; ?>" class="politicas">Avisos de privacidad.</a>
				<a href="<?php echo isset($options['web_termCond_id'])?get_post($options['web_termCond_id'])->guid:'#'; ?>" class="terminos">Términos y condiciones.</a>
			
			</div>
		</div>
		
	</div>
</div>
</body>
</html>