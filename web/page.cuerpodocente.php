<?php



function getArrayBreadCrumbs($id, $id_home){
	$breadcrumbs=array();
	$page=get_page( $id );
	if($page->post_parent and $page->ID!=$id_home)
		$breadcrumbs=getArrayBreadCrumbs($page->post_parent, $id_home);
	if($page->ID==$id_home) $breadcrumbs[$page->ID]='<li property="itemListElement" typeof="ListItem">
            		<a property="item" typeof="WebPage" href="'.$page->guid.'">Home</a></li>';
					else	$breadcrumbs[$page->ID]='<li property="itemListElement" typeof="ListItem">
            		<a property="item" typeof="WebPage" href="'.$page->guid.'">'.$page->post_title.'</a></li>';
	return $breadcrumbs;
}

// carga las opciones de la plantilla
$options=get_option('pa_opciones');	
	
$current_ID=get_the_ID();	
	
$page=get_page( $current_ID );
$page_vars=get_post_custom( $page->ID );
$breadcrumsArray=getArrayBreadCrumbs( $page->ID, $options['web_menu_id'] );
$depth=count($breadcrumsArray); foreach($breadcrumsArray as $k=>$v); $breadcrumsArray[$k]=implode(' class="active" >',explode('>',$breadcrumsArray[$k]));

	// toma el listado de páginas
	$childs = get_pages( array('parent' => $page->ID, 'sort_order' => 'ASC', 'sort_column' => 'menu_order' ) );
	if(count($childs)==0){
		$page=get_page( $page->post_parent );
		$page_vars=get_post_custom( $page->ID );
		$breadcrumsArray=getArrayBreadCrumbs( $page->ID, $options['web_menu_id'] );
		$depth=count($breadcrumsArray);
	}
?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/web/css/cuerpodocente.css">

	<div id="primary" class="content-area">
		<div id="pagecontent" class="site-content" role="main">

			<ol vocab="http://schema.org/" typeof="BreadcrumbList" class="breadcrumbs">
            	<li property="itemListElement" typeof="ListItem">
            		<a property="item" typeof="WebPage" href="<?php echo get_site_url(); ?>" >
                    <span property="name">HOME</span>
                    </a>
                    <meta property="position" content="1">
                    <span>&gt;</span>
				</li>
				<?php echo implode('<span>&gt;</span>', $breadcrumsArray ); ?>
            </ol>
			
			
			<div class="cuerpodocente">
				<div class="cuerpodocente_title"><?php echo isset($page_vars['HtmHeaderWeb'])?$page_vars['HtmHeaderWeb'][0]:'Cuerpo Docente'; ?></div>
					<div class="content">
						<div class="subTitle">Te presentamos a nuestros expertos internacionales.</div>
						<div class="content_content">
							<ul class="docentes" id="docentes">
						<?php
							// toma el listado de páginas
							$childs = get_pages( array('parent' => $page->ID, 'sort_order' => 'ASC', 'sort_column' => 'menu_order' ) );
							// recorre la lista de páginas
							foreach($childs as $k=>$child){
								// tova las variables configurables de cáda págia
								$vars=get_post_custom($child->ID); 
								$href=isset($vars['link-href'])? current($vars['link-href']): $child->guid;
								// dibuja la liga ?>
								<li data-id="<?php echo $child->ID; ?>" >
									<div class="data_preview">
										<div class="img"><img src="<?php echo isset($vars['image'])?$vars['image'][0]:''; ?>"></div>
										<div class="name"><?php echo $child->post_title; ?></div>
										<div class="title"><?php echo isset($vars['title'])?$vars['title'][0]:'&nbsp;'; ?></div>
										<div class="countries">
											<?php
											if(isset($vars['countries'])){
												$vars['countries'][0]=explode(',',$vars['countries'][0]);
												foreach ($vars['countries'][0] as $contry) {
													?><span class="f_<?php echo  $contry; ?>"></span><?php
												}
											}
											?>
										</div>
									</div>
									<div class="data_content" style="display:none;">
										<div class="background"></div>
										<div class="content_content">
											<div class="img"><img src="<?php echo isset($vars['image'])?$vars['image'][0]:''; ?>"></div>
											<div class="name"><?php echo $child->post_title; ?></div>
											<div class="title"><?php echo isset($vars['title'])?$vars['title'][0]:'&nbsp;'; ?>
												<div class="countries">
													<?php
													if(isset($vars['countries'])){
														foreach ($vars['countries'][0] as $contry) {
															?><span class="f_<?php echo  $contry; ?>"></span><?php
														}
													}
													?>
												</div>
											</div>
											<div class="description">
												<?php echo $child->post_content; ?>
												<div class="clearBoth"></div>
											</div>
											<a class="link" href="<?php echo isset($vars['link-href'])?$vars['link-href'][0]:'#'; ?>">
												<?php echo isset($vars['link-title'])?$vars['link-title'][0]:'Ver sus artículos culinarios'; ?>
											</a>
											<a class="close"></a>
										</div>
									</div>
								</li><?php
							}
						?>
							</ul>
						</div>
					</div>





				<div class="clearBoth"></div>
				
			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<script language="javascript">
$(function(){
	var _docente= $('#docentes');
	var _lis=_docente.children();
	var _cnt=$('.data_content',_lis);
	var currentScrollTop;
	_lis.click(function(){
		currentScrollTop=$(window).scrollTop();
		
		_lis.find('.data_preview').css({ marginBottom: '' }); // Cierra persiana. regresa todos los margenes a su tamaño original
		var me=$(this);
		_lis.each(function(){
			$(this).removeClass('hover');
		});
		me.addClass('hover');
		me.css({left:me.offset().left-_docente.offset().left});
		var _bg=$('.background',me), _cnt=$('.data_content',me), content_content=$('.content_content',me);
		
		_cnt.css({ left:0  });
		var __left=_docente.offset().left-_cnt.offset().left;
		_cnt.css({ left:__left  });
		
		var _ww=$(window).width(), _cw=_cnt.width(), _ch=_cnt.height();
		var plus=_cnt.offset().left-(_ww-_cw)/2
		var _left=-(_ww-_cw)/2-plus;
		_bg.css({ width:_ww, height:'100%', left:_left});
		
		
		content_content.stop().css({height:0}).stop().animate({height:'100%'},300, function(){
			$("html, body").stop().animate({ scrollTop:_cnt.offset().top-100});
			//me.find('.data_preview').css({ marginBottom: $(this).height()+100 }); // abre persiana.
		});
	});
	_cnt.click(function(event){
		event = event || window.event;
		event.stopPropagation();
	});
	_lis.filter('[data-id="<?php echo $current_ID; ?>"]').click();
	$('.close').click(function(){
		_lis.filter('.hover').find('.data_preview').stop().css({ marginBottom: '' });
		$("html, body").stop().animate({ scrollTop:currentScrollTop});
		var me=$(this);
		_lis.each(function(){
			$(this).removeClass('hover');
		});
	});
})
</script>