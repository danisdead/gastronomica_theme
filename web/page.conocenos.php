<?php



function getArrayBreadCrumbs($id, $id_home){
	$breadcrumbs=array();
	$page=get_page( $id );
	if($page->post_parent and $page->ID!=$id_home)
		$breadcrumbs=getArrayBreadCrumbs($page->post_parent, $id_home);
	if($page->ID==$id_home) $breadcrumbs[$page->ID]='<li property="itemListElement" typeof="ListItem">
            		<a property="item" typeof="WebPage" href="'.$page->guid.'">Home</a></li>';
					else	$breadcrumbs[$page->ID]='<li property="itemListElement" typeof="ListItem">
            		<a property="item" typeof="WebPage" href="'.$page->guid.'">'.$page->post_title.'</a></li>';
	return $breadcrumbs;
}


// carga las opciones de la plantilla
$options=get_option('pa_opciones');	
	
$page=get_page( get_the_ID() );
$page_vars=get_post_custom( get_the_ID() );
$breadcrumsArray=getArrayBreadCrumbs( $page->ID, $options['web_menu_id'] );
$depth=count($breadcrumsArray); foreach($breadcrumsArray as $k=>$v); $breadcrumsArray[$k]=implode(' class="active" >',explode('>',$breadcrumsArray[$k]));
?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/web/css/conocenos.css">

	<div id="primary" class="content-area">
		<div id="pagecontent" class="site-content" role="main">

			<ol vocab="http://schema.org/" typeof="BreadcrumbList" class="breadcrumbs">
            	<li property="itemListElement" typeof="ListItem">
            		<a property="item" typeof="WebPage" href="<?php echo get_site_url(); ?>" >
                    <span property="name">HOME</span>
                    </a>
                    <meta property="position" content="1">
                    <span>&gt;</span>
				</li>
				<?php echo implode('<span>&gt;</span>', $breadcrumsArray ); ?>
            </ol>
			
			<div class="conocenos">

<?php
			$parent=get_page($page->post_parent); 
			$parent_vars=get_post_custom( $parent->ID );
			// toma el listado de páginas
			$siblings = get_pages( array('parent' => $page->post_parent, 'sort_order' => 'ASC', 'sort_column' => 'menu_order' ) );
			// busca la posición de la página actual
			foreach($siblings as $k=>$sibling) if($sibling->ID==$page->ID) {
				$prev=isset($siblings[$k-1])? $siblings[$k-1]: $siblings[count($siblings)-1];
				$next=isset($siblings[$k+1])? $siblings[$k+1]: $siblings[0];
				break;
			}
		?>
				<div class="nivel2">
					<div class="conocenos_title"><?php echo isset($parent_vars['HtmHeaderWeb'])?$parent_vars['HtmHeaderWeb'][0]:$parent->post_title; ?></div>
					<div class="conocenos_content"><?php echo isset($page_vars['title'][0])?$page_vars['title'][0]:($parent->ID!=$page->ID?$parent->post_content:''); ?></div>
					<div class="content">
						<? if($parent->ID!=$page->ID){ ?><div class="subTitle"><?php echo $page->post_title; ?></div><?php } ?>
						<div class="content_content textContent">
							<?php echo $page->post_content; ?>
						</div>
					</div>
					<ul class="submenu" >
						<div class="title"><?php echo $parent->post_title; ?></div>
						<?php
							// toma el listado de páginas
							$childs = get_pages( array('parent' => $parent->ID, 'sort_order' => 'ASC', 'sort_column' => 'menu_order' ) );
							// recorre la lista de páginas
							foreach($childs as $k=>$child){
								// tova las variables configurables de cáda págia
								$vars=get_post_custom($child->ID); 
								$href=isset($vars['link-href'])? current($vars['link-href']): $child->guid;
								// dibuja la liga ?>
								<li>
									<a href="<?php echo $href; ?>" class="<?php echo $child->ID==$page->ID? 'active': ''; ?>" >
										<span><?php echo $child->post_title; ?></span>
									</a>
								</li><?php
							}
						?>
					</ul>
				</div>


				<div class="clearBoth"></div>
				
			</div>

		</div><!-- #content -->
	</div><!-- #primary -->
