<?php



function getArrayBreadCrumbs($id, $id_home){
	$breadcrumbs=array();
	$page=get_page( $id );
	if($page->post_parent and $page->ID!=$id_home)
		$breadcrumbs=getArrayBreadCrumbs($page->post_parent, $id_home);
	if($page->ID==$id_home) $breadcrumbs[$page->ID]='<a href="'.$page->guid.'">Home</a>';
					else	$breadcrumbs[$page->ID]='<a href="'.$page->guid.'">'.$page->post_title.'</a>';
	return $breadcrumbs;
}


// carga las opciones de la plantilla
$options=get_option('pa_opciones');	
	
$page=get_page( get_the_ID() );
$page_vars=get_post_custom( get_the_ID() );
$breadcrumsArray=getArrayBreadCrumbs( $page->ID, $options['web_menu_id'] );
$depth=count($breadcrumsArray); $breadcrumsArray[$depth-2]=implode(' class="active" >',explode('>',$breadcrumsArray[$depth-2]));
?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/web/css/contacto.css">

	<div id="primary" class="content-area">
		<div id="pagecontent" class="site-content" role="main">

			<div class="breadcrumbs"><a href="/">HOME</a><span>&gt;</span><?php echo implode('<span>&gt;</span>', $breadcrumsArray ); ?></div>
			
			<div class="contacto">
				<div class="nivel2">
					<div class="contacto_title"><?php echo isset($page_vars['HtmHeaderWeb'])?$page_vars['HtmHeaderWeb'][0]:$page->post_title; ?></div>
					<div class="contacto_content"><?php echo isset($page_vars['title'][0])?$page_vars['title'][0]:$page->post_content; ?></div>
					<div class="content">
						
						<div class="subtitle">Descubre el mundo de la gastronomía</div>
						<div class="page_content">
						<?php echo $page->post_content; ?>
						</div>
						
						
				<div class="main_contact">
					<div class="main_contact_content">	
						<form method="POST" >
							<div class="field _field_name">
								<label for="_field_name" >Nombre</label>
								<input type="text" id="_field_name" name="field_name" placeholder="Nombre completo" data-expreg="[A-Za-z ÑñÁáÉéÍíÓóÚúü]{3,}" data-only="[A-Za-z ÑñÁáÉéÍíÓóÚúü]" />
								<span class="error">Indique su nombre completo.</span>
							</div>
							<div class="field _field_email">
								<label for="_field_email" >Correo electrónico</label>
								<input type="text" id="_field_email" name="field_email" placeholder="ejemplo@ejemplo.com" data-expreg="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$" data-only="[a-zA-Z0-9\.-@]" />
								<span class="error">Correo electrónico inválido.</span>
							</div>
							<div class="field _field_telefono">
								<label for="_field_telefono" >Teléfono</label>
								<input type="text" id="_field_telefono" name="field_telefono" placeholder="Teléfono" data-expreg="^([0-9]{7,15})$" maxlength="15" data-only="[0-9]" />
								<span class="error">Use sólo números.</span>
								<input type="hidden" id="_field_lada" name="field_lada" placeholder="Lada" value="52" />
								<div id="_ladaSelector">
									<div class="current"><span class="ico"></span><span class="title"></span></div>
									<ul>
										<li class="mex" data-val="52"><span class="ico"></span><span class="title">México +52</span></li>
										<li class="usa" data-val="1"><span class="ico"></span><span class="title">Estados Unidos +1</span></li>
										<li class="esp" data-val="34"><span class="ico"></span><span class="title">España +34</span></li>
										<li class="vzl" data-val="58"><span class="ico"></span><span class="title">Venezuela +58</span></li>
										<li class="pru" data-val="51"><span class="ico"></span><span class="title">Perú +51</span></li>
										<li class="col" data-val="56"><span class="ico"></span><span class="title">Colombia +56</span></li>
										<li class="arg" data-val="54"><span class="ico"></span><span class="title">Argentina +54</span></li>
										<li class="otr"><span class="ico"></span><span class="title">Otro +</span><input type="text" id="lada_otro" value="00" maxlength="2"  data-only="[0-9]" ></li>
									</ul>
								</div>
							</div>
							
							<div class="field _field_phon_type">
								<input type="radio" id="_field_phon_type_local" name="field_phon_type" checked="checked" value="home"/>
								<label for="_field_phon_type_local" class='radio_lbl' >Fijo</label>
								<input type="radio" id="_field_phon_type_mbile" name="field_phon_type" value="mobile"/>
								<label for="_field_phon_type_mbile" class='radio_lbl' >Celular</label>
								<div class="clearBoth"></div>
							</div>
							<div class="field _field_area">
								<label for="_field_area" >Área de interés</label>
								<select id="_field_area" name="field_area">
									<option value="No seleccionó">Área de interés</option>
									<option value="Repostería Profesional">Repostería Profesional</option>
									<option value="El Mundo del Vino">El Mundo del Vino</option>
									<option value="Cocina Profesional">Cocina Profesional</option>
									<option value="Nutrición">Nutrición</option>
									<option value="Gestión Gastronómica">Gestión Gastronómica</option>
								</select>
							</div>
							<div class="field _field_questions">
								<label for="_field_questions" >Escríbenos tus dudas</label>
								<textarea id="_field_questions" name="field_questions" rows="4" placeholder="Escríbenos tus dudas"></textarea>
							</div>
							<div class="submit submit_fast">
								<input type="hidden" name="submit_fast" value="1" />
								<input type="submit" id="_submit_fast" value="Solicitar información" />
							</div>
						</form>
					</div>
				</div>
						
						
						


				<div class="contact_contries">
					<div class="contry">
						<div class="title">México</div>
						<div class="phone">+52 55 5207 5443 / 7485</div>
						<div class="address">Av. Paseo de la Reforma No.404,<br>Col. Juárez, Cuauhtémoc,<br>DF, México.</div>

						<div class="title">Estados Unidos</div>
						<div class="phone">+1 800 864 4107</div>
						<div class="address">55 Merrick Way, Suite 216,<br>Coral Gables, 33134, Florida, USA.</div>

						<div class="title">Colombia</div>
						<div class="phone">+56 2 2570 9502</div>

						<div class="title">España</div>
						<div class="phone">+34 9 1123 8716</div>

						<div class="title">Argentina</div>
						<div class="phone">+54 11 5217 5945</div>

						<div class="title">Email</div>
						<div class="email">info@gastronomicainternacional.com</div>
						
						<div class="title">Skype</div>
						<div class="email">gastronomica.internacional</div>
					</div>					
				</div>
						
						

	
						
						
						
					</div>
					<ul class="submenu" >
						<div class="title">Síguenos</div>
						<div class="contact_social">
							<a target="_blank" href="//www.facebook.com/GastronomicaInternacional" _href="//www.facebook.com/sharer.php?s=100&p[url]=<?php echo get_permalink(); ?>" class="facebook">facebook</a>
							<a target="_blank" href="//www.youtube.com/channel/UC9xKIOOQDEyL5d1xxYJgkOQ" class="youtube">youtube</a>
							<a target="_blank" href="//instagram.com/_gastronomica" class="instagram">instagram</a>
							<a target="_blank" href="//twitter.com/@_gastronomica" class="twitter">twitter</a>
						</div>
					</ul>
				</div>


				<div class="clearBoth"></div>
				
			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

	
	<script>
$(function(){
	$('.fast_contact').css({display:'none'});
	
	// lada
	
	var _field_lada=$('#_field_lada');
	var _ladaSelector=$('#_ladaSelector');
	var _current=$('.current',_ladaSelector),
		_ul=$('ul',_ladaSelector),
		_li=$('li',_ladaSelector),
		_input=$('input',_ladaSelector);
	var _current_ico=$('.ico',_current),
		_current_title=$('.title',_current);
	_ladaSelector.parents('form').find('input').on("keyup keypress", function(e) {
	  var code = e.keyCode || e.which; 
	  if (code  == 13) { event.stopPropagation ? event.stopPropagation() : (event.cancelBubble=true); return false; }
	});/*.focus(function(){
		if(!_fast_contact_dropdown.is('.up')) _fast_contact_dropdown.click();
	});*/
	_current.click(function(){
		_ul.toggle();
	});
	_li.click(function(){
		var $this=$(this);
		if($this.is('[data-val]')){
			_field_lada.val($this.attr('data-val'));
			_current_ico.css('backgroundPosition',$this.find('.ico').css('backgroundPosition'));
			_current_title.css('backgroundPosition','-100px -100px');
			_current_title.text('+'+$this.attr('data-val'));
			_ul.hide();
		}
	});
	_input.click(function(e){
		var event = e || window.event;
		event.stopPropagation ? event.stopPropagation() : (event.cancelBubble=true);
		$(this).select();
	}).on("keyup keypress", function(e) {
		var $this=$(this);
		var code = e.keyCode || e.which; 
		if (code  == 13) {
			_field_lada.val($this.val());
			_current_ico.css('backgroundPosition',$this.siblings('.ico').css('backgroundPosition'));
			_current_title.css('backgroundPosition','-100px -100px');
			_current_title.text('+'+$this.val());
			_ul.hide();
		}
	}).change(function(){
		var $this=$(this);
		_field_lada.val($this.val());
		_current_ico.css('backgroundPosition',$this.siblings('.ico').css('backgroundPosition'));
		_current_title.css('backgroundPosition','-100px -100px');
		_current_title.text('+'+$this.val());
		_ul.hide();
	});
	_ladaSelector.mouseleave(function(){ _ul.hide(); });
	
	
			// SUBMIT
				var main_contact=$('.main_contact');
				var btn_submit=$('#_submit_fast',main_contact);
				var fields_exp=$('[data-expreg]',main_contact);
				fields_exp.each(function(){
					$(this).change(function(){
						var me=$(this);
						var _data_expreg=me.attr('data-expreg');
						var expreg=new RegExp(_data_expreg);
						if(!expreg.test(me.val())){
							me.css({ color: 'red' });
							me.focus();
							me.siblings('.error').show();
						}else{
							me.css({ color: 'black' });
							me.siblings('.error').hide();
						}
						me.val(me.val().replace(/\s\s+/g, ' ').trim());
					});
				});
				$('[data-only]',main_contact).on("keyup paste", function(){
					var me=$(this);
					if(me.attr('data-only')!='')
						setTimeout(jQuery.proxy(function() {
							var expreg=new RegExp(me.attr('data-only'));
							for(var i=0; i<me.val().length; i++)
								if(!expreg.test(me.val().substring(i,i+1))){
									me.val(me.val().substring(0,i)+me.val().substring(i+1)); i--;
								}
							me.val(me.val().replace(/\s\s+/g, ' '));
						}), 0);
				});
				var form=btn_submit.parents('form')
				form.submit(function(){
					fields_exp.each(function(){ $(this).change(); });
					if( $('span.error:visible').length==0 ){
						var width=btn_submit.parent().width();
						var interval;
						btn_submit.css('text-indent',-1000).animate({paddingTop:0,paddingBottom:0},500,function(){
							btn_submit.css({left:-width,backgroundColor:'green'});
							var i=0;
							interval=setInterval(function(){ i++; btn_submit.stop().animate({left:~~(-width/i)}); },100);
						});
						$.post('/',form.serialize(),function(data){
							setTimeout(function(){
								clearInterval(interval);
								btn_submit.stop().css({left:0}).animate({opacity:0},500,function(){
									if(data=='1') { form[0].reset(); $('input, textarea',form).placeholder(); }
									btn_submit.stop().css({ backgroundColor:'transparent'}).addClass(data=='1'?'true':'false').animate({paddingTop:8,paddingBottom:8},500, function(){
										btn_submit.stop().animate({opacity:1},500, function(){
											setTimeout(function(){
												btn_submit.stop().animate({opacity:0},500, function(){
													btn_submit.stop().css({ textIndent:0}).removeClass('true').removeClass('false')
													.animate({opacity:1},500);
												});
											},1000);
										});
									});
								});
							},500)
							/*$.colorbox({html:(
								data=='1'?
								'<h1>Tu mesaje fue enviado</h1>':
								'<h1>Error	</h1>'
							)});*/
						});
					}
					return false;
				});
				
				$('input, textarea',form).placeholder();
				$('#_field_area').change(function(){
					$(this).css({ color: ($(this).find(':selected').index()==0?'#aaa':'#000') });
				}).focus(function(){
					$(this).css({color:'#000'});
				}).blur(function(){
					$(this).css({ color: ($(this).find(':selected').index()==0?'#aaa':'#000') });
				}).css({color:'#aaa'});
});
	</script>